Rails.application.routes.draw do
  devise_for :users, controllers:{
      sessions: 'authentication/sessions'
  }
  mount V2::Root => '/'
  devise_scope :user do
    authenticated :user do
      root 'main#home', as: :authenticated_root
      get 'calendarApp/', to: 'main#calendarApp' #ruta para el componente CalendarApp
      #Api Routes
      namespace :api do
        namespace :v1 do
          resources :users, only: [:index,:show, :create, :destroy, :update] # usuarios del sistema
          get 'list_shifts', to: 'list_shift#index'
          get 'list_departments', to: 'data_sets#index'
          get 'list_departments/:id', to: 'data_sets#show'
          get 'list_benefits', to: 'benefits#index'
          get 'list_benefits/:id', to: 'benefits#show'

        end
      end
      # web routes
      namespace :web do
        resources :contracts, :benefits, :departments, :shifts, :employees, :users, :areas, :calendars, :holidays
        get 'calendar_programmed_vs_real', to: 'calendar_trace#index'
        get 'kpi_productivity', to: 'calendar_trace#assistance'
        get 'update_holidays', to: 'holidays#refill', via: [:post]
      end
    end

  #Api Routes
  namespace :api do
      devise_for :users
    namespace :v1 do
      resources :calendars , param: :data
      resources :users, only: [:index,:show, :create, :destroy, :update] # usuarios del sistema
      get 'list_shifts', to: 'list_shift#index'
      get 'list_shifts/:time', to: 'list_shift#search_shift_type'
      get 'list_departments', to: 'data_sets#departments'
      get 'list_areas', to: 'data_sets#index'
      get 'list_areas/:id', to: 'data_sets#show'
      get 'list_benefits', to: 'benefits#index'
      get 'list_contracts', to: 'contracts#index'
      get 'list_benefits/:id', to: 'benefits#show'
      get 'List_calendars_code/:code', to: 'calendars#calendar_code'
      get 'list_calendar_logs', to: 'calendar_logs#index'
      get 'list_calendar_logs/:code', to: 'calendar_logs#show'
      get 'list_calendar_logs_dep/:department_id', to: 'calendar_logs#logs_dep'
      get 'list_calendar_logs_date/:date', to: 'calendar_logs#logs_date'
      get 'list_holidays', to: 'holidays#index'
      get 'list_holidays_year/:year', to: 'holidays#list_year'
      get 'list_holidays_month_year/:month/:year', to: 'holidays#list_month_year'
    end
  end
end

  # ========== Kit coding ========== #

  root 'main#home'
  get '*path' => 'main#err'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
