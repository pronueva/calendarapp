source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.2'
# Use sqlite3 as the database for Active Record
#gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
# Devise is flexible authentication solution for Rails with Warden.
gem 'devise'
# Webpacker plugin to integrate React in your Rails application
gem 'webpacker-react', '0.3.2'
# haml-rails
gem 'haml-rails'
# Annotate to show columns on model file
gem 'annotate'
# Responders for api
gem 'responders'
gem 'rack-cors'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# jQuery UI for the Rails asset pipeline
gem 'jquery-ui-rails'
# Js routes brings Rails named routes to javascript
gem 'js-routes'
# jQuery plugin for drop-in fix binded events problem caused by Turbolinks
gem 'jquery-turbolinks'
# Use Awesome icons mada faca
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.2'
# Use for creating and managing a breadcrumb navigation.
gem 'breadcrumbs_on_rails'
# will_paginate provides a simple API for performing paginated queries with Active Record, DataMapper and Sequel, and includes helpers for rendering pagination links in Rails, Sinatra and Merb web apps.
gem 'will_paginate', '~> 3.1', '>= 3.1.6'
# Pg is the Ruby interface to the PostgreSQL RDBM
gem 'pg'
# collecting Locale data for Ruby on Rails I18n as well as other interesting, Rails related I18n stuff http://rails-i18n.org
gem 'rails-i18n'
# Api gems:
# An opinionated framework for creating REST-like APIs in Ruby.
gem 'grape'
# An API focused facade that sits on top of an object model.
gem 'grape-entity','0.6.1'
# Create beautiful JavaScript charts with one line of Ruby
gem 'chartkick'
# Rails Action Mailer adapter for Mailgun
gem 'mailgun_rails'
# JSON implementation for Ruby
gem 'json'
# Makes http fun again! ja x2
gem 'httparty'
# PDF generator (from HTML) plugin for Ruby on Rails
gem 'wicked_pdf'
# Provides binaries for WKHTMLTOPDF project in an easily accessible package.
gem 'wkhtmltopdf-binary'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
  gem 'bullet'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

