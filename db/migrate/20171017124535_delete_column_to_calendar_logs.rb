class DeleteColumnToCalendarLogs < ActiveRecord::Migration[5.1]
  def change
    remove_column :calendar_logs, :status, :integer
  end
end
