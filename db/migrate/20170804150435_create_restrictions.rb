class CreateRestrictions < ActiveRecord::Migration[5.1]
  def change
    create_table :restrictions do |t|
      t.string :cod,            null: false, default: ''
      t.string :name,           null: false, default: ''
      t.string :restrict_type,  null: false, default: ''
      t.text :description
      t.timestamps
    end
  end
end
