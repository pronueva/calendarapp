class AddCodeToCalendars < ActiveRecord::Migration[5.1]
  def change
    add_column :calendars, :code, :string
  end
end
