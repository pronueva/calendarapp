class CreateHolidayRecords < ActiveRecord::Migration[5.1]
  def change
    create_table :holiday_records do |t|
      t.references :holiday, foreign_key: true
      t.references :employee, foreign_key: true
      t.boolean :status
      t.date :offset
    end
  end
end
