class AddForeignKeysToCalendars < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :calendars, :employees
    add_foreign_key :calendars, :users
    add_foreign_key :calendars, :shifts
  end
end
