class CreateCalendarLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :calendar_logs do |t|
      t.string :code
      t.date :date
      t.integer :status
      t.integer :count_errors
      # foreign key ->
      t.integer :department_id, null: true

      t.timestamps
    end
  end
end
