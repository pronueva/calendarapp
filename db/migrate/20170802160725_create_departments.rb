class CreateDepartments < ActiveRecord::Migration[5.1]
  def change
    create_table :departments do |t|
      t.string :name,         null: false, default: ''
      t.string :area,         null: false, default: ''

      ## Foreing
      t.references :id_branch,    foreing_key:true
      t.timestamps
    end
  end
end
