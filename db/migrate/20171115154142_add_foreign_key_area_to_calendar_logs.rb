class AddForeignKeyAreaToCalendarLogs < ActiveRecord::Migration[5.1]
  def change
    add_reference :calendar_logs, :area, foreign_key: true
  end
end
