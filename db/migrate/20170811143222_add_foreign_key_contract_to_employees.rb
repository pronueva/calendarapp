class AddForeignKeyContractToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :employees, :contracts
    add_foreign_key :employees, :benefits
  end
end
