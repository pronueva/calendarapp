class AddForeignKeyAreaToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_reference :employees, :area, foreign_key: true
  end
end
