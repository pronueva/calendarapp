class CreateCalendarsRestrictions < ActiveRecord::Migration[5.1]
  def change
    create_table :calendars_restrictions, id: false do |t|
      t.integer :calendar_id,     index: true
      t.integer :restriction_id, index: true
      t.timestamps
    end
    add_foreign_key :calendars_restrictions, :calendars
    add_foreign_key :calendars_restrictions, :restrictions
  end
end
