class AddDepartmentIdToCalendarLog < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :calendar_logs, :departments
  end
end
