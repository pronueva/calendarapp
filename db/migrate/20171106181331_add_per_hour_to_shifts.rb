class AddPerHourToShifts < ActiveRecord::Migration[5.1]
  def change
    add_column :shifts, :per_hour, :integer
  end
end
