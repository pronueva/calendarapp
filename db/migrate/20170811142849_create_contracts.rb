class CreateContracts < ActiveRecord::Migration[5.1]
  def change
    create_table :contracts do |t|
      t.string :contract_type
      t.text :description
      t.timestamps
    end
  end
end
