class RemoveAllForeignKeyToDepartments < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :employees, :departments
    remove_foreign_key :calendar_logs, :departments
  end
end
