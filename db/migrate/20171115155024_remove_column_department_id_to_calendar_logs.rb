class RemoveColumnDepartmentIdToCalendarLogs < ActiveRecord::Migration[5.1]
  def change
    remove_column :calendar_logs, :department_id, :integer
  end
end
