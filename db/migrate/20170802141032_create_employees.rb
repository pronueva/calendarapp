class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :first_name,     null: false, default: ''
      t.string :last_name,      null: false, default: ''
      t.string :rut,            null: false, default: '', :limit => 13
      t.string :email,          null: false, default: ''
      t.string :tel,            null: false, default: ''
      # foreign key ->
      t.integer :department_id, null: true
      t.integer :address_id,    null: true
      t.integer :contract_id,   null: true
      t.integer :benefit_id,    null: true

      t.timestamps
    end
  end
end
