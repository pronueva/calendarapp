class CreateShifts < ActiveRecord::Migration[5.1]
  def change
    create_table :shifts do |t|
      t.string :cod,          null: false, default: ''
      t.time :shift_start,    null: false, default: '00:00:00'
      t.time :shift_end,      null: false, default: '00:00:00'
      t.time :break_start,    null: false, default: '00:00:00'
      t.time :break_end,      null: false, default: '00:00:00'
      t.string :shift_type,         null: false, default: ''
      t.string :description,  null: true

      t.timestamps
    end
  end
end
