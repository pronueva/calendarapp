class AddForeignKeyToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :employees, :departments
  end
end
