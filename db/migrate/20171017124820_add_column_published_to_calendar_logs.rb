class AddColumnPublishedToCalendarLogs < ActiveRecord::Migration[5.1]
  def change
    add_column :calendar_logs, :published, :boolean
  end
end
