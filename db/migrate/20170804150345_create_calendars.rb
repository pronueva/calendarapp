class CreateCalendars < ActiveRecord::Migration[5.1]
  def change
    create_table :calendars do |t|
      t.references :user,      index: true, null: false, default: ''
      t.references :employee,  index: true, null: false, default: ''
      t.references :shift,     index: true, null: false, default: ''
      t.date :date
      t.boolean :published
      t.timestamps
    end
  end
end
