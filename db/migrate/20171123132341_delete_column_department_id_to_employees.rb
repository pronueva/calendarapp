class DeleteColumnDepartmentIdToEmployees < ActiveRecord::Migration[5.1]
  def change
    remove_column :employees, :department_id, :integer
  end
end
