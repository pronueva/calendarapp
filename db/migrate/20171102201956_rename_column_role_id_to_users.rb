class RenameColumnRoleIdToUsers < ActiveRecord::Migration[5.1]
  def change
    #remove_foreign_key :users, column: :role_id
    rename_column :users, :role_id, :role
  end
end
