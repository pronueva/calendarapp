class AddDefaultRoleToUsers < ActiveRecord::Migration[5.1]
  def change
    def up
      change_column :users, :role, :integer, default: 2
    end

    def down
      change_column :users, :role, :integer
    end
  end
end
