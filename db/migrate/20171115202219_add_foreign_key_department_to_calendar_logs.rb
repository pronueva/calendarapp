class AddForeignKeyDepartmentToCalendarLogs < ActiveRecord::Migration[5.1]
  def change
    add_reference :calendar_logs, :department, foreign_key: true
  end
end
