class RenameColumAreaFromDepartment < ActiveRecord::Migration[5.1]
  def change
    rename_column :departments, :area , :description
  end
end
