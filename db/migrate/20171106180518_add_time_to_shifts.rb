class AddTimeToShifts < ActiveRecord::Migration[5.1]
  def change
    add_column :shifts, :time, :integer, default:1
  end
end
