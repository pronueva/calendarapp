# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171212173519) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.bigint "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_areas_on_department_id"
  end

  create_table "benefits", force: :cascade do |t|
    t.string "cod"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calendar_logs", force: :cascade do |t|
    t.string "code"
    t.date "date"
    t.integer "count_errors"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "published"
    t.bigint "area_id"
    t.bigint "department_id"
    t.index ["area_id"], name: "index_calendar_logs_on_area_id"
    t.index ["department_id"], name: "index_calendar_logs_on_department_id"
  end

  create_table "calendars", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "employee_id", null: false
    t.bigint "shift_id", null: false
    t.date "date"
    t.boolean "published"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.index ["employee_id"], name: "index_calendars_on_employee_id"
    t.index ["shift_id"], name: "index_calendars_on_shift_id"
    t.index ["user_id"], name: "index_calendars_on_user_id"
  end

  create_table "calendars_restrictions", id: false, force: :cascade do |t|
    t.integer "calendar_id"
    t.integer "restriction_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["calendar_id"], name: "index_calendars_restrictions_on_calendar_id"
    t.index ["restriction_id"], name: "index_calendars_restrictions_on_restriction_id"
  end

  create_table "contracts", force: :cascade do |t|
    t.string "contract_type"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.bigint "id_branch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_branch_id"], name: "index_departments_on_id_branch_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "rut", limit: 13, default: "", null: false
    t.string "email", default: "", null: false
    t.string "tel", default: "", null: false
    t.integer "address_id"
    t.integer "contract_id"
    t.integer "benefit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "area_id"
    t.index ["area_id"], name: "index_employees_on_area_id"
  end

  create_table "holiday_records", force: :cascade do |t|
    t.bigint "holiday_id"
    t.bigint "employee_id"
    t.boolean "status"
    t.date "offset"
    t.index ["employee_id"], name: "index_holiday_records_on_employee_id"
    t.index ["holiday_id"], name: "index_holiday_records_on_holiday_id"
  end

  create_table "holidays", force: :cascade do |t|
    t.date "date"
    t.string "name"
    t.text "description"
  end

  create_table "restrictions", force: :cascade do |t|
    t.string "cod", default: "", null: false
    t.string "name", default: "", null: false
    t.string "restrict_type", default: "", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shifts", force: :cascade do |t|
    t.string "cod", default: "", null: false
    t.time "shift_start", default: "2000-01-01 00:00:00", null: false
    t.time "shift_end", default: "2000-01-01 00:00:00", null: false
    t.time "break_start", default: "2000-01-01 00:00:00", null: false
    t.time "break_end", default: "2000-01-01 00:00:00", null: false
    t.string "shift_type", default: "", null: false
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "time", default: 1
    t.integer "per_hour"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "rut", limit: 13, default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.bigint "role"
    t.bigint "branch_office_id"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "api_key"
    t.index ["branch_office_id"], name: "index_users_on_branch_office_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role"], name: "index_users_on_role"
  end

  add_foreign_key "areas", "departments"
  add_foreign_key "calendar_logs", "areas"
  add_foreign_key "calendar_logs", "departments"
  add_foreign_key "calendars", "employees"
  add_foreign_key "calendars", "shifts"
  add_foreign_key "calendars", "users"
  add_foreign_key "calendars_restrictions", "calendars"
  add_foreign_key "calendars_restrictions", "restrictions"
  add_foreign_key "employees", "areas"
  add_foreign_key "employees", "benefits"
  add_foreign_key "employees", "contracts"
  add_foreign_key "holiday_records", "employees"
  add_foreign_key "holiday_records", "holidays"
end
