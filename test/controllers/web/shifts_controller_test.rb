require 'test_helper'

class Web::ShiftsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_shift = web_shifts(:one)
  end

  test "should get index" do
    get web_shifts_url
    assert_response :success
  end

  test "should get new" do
    get new_web_shift_url
    assert_response :success
  end

  test "should create web_shift" do
    assert_difference('Web::Shift.count') do
      post web_shifts_url, params: { web_shift: {  } }
    end

    assert_redirected_to web_shift_url(Web::Shift.last)
  end

  test "should show web_shift" do
    get web_shift_url(@web_shift)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_shift_url(@web_shift)
    assert_response :success
  end

  test "should update web_shift" do
    patch web_shift_url(@web_shift), params: { web_shift: {  } }
    assert_redirected_to web_shift_url(@web_shift)
  end

  test "should destroy web_shift" do
    assert_difference('Web::Shift.count', -1) do
      delete web_shift_url(@web_shift)
    end

    assert_redirected_to web_shifts_url
  end
end
