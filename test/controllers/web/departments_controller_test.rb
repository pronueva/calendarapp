require 'test_helper'

class Web::DepartmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_department = web_departments(:one)
  end

  test "should get index" do
    get web_departments_url
    assert_response :success
  end

  test "should get new" do
    get new_web_department_url
    assert_response :success
  end

  test "should create web_department" do
    assert_difference('Web::Department.count') do
      post web_departments_url, params: { web_department: {  } }
    end

    assert_redirected_to web_department_url(Web::Department.last)
  end

  test "should show web_department" do
    get web_department_url(@web_department)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_department_url(@web_department)
    assert_response :success
  end

  test "should update web_department" do
    patch web_department_url(@web_department), params: { web_department: {  } }
    assert_redirected_to web_department_url(@web_department)
  end

  test "should destroy web_department" do
    assert_difference('Web::Department.count', -1) do
      delete web_department_url(@web_department)
    end

    assert_redirected_to web_departments_url
  end
end
