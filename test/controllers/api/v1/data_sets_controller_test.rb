require 'test_helper'

class Api::V1::DataSetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @api_v1_data_set = api_v1_data_sets(:one)
  end

  test "should get index" do
    get api_v1_data_sets_url, as: :json
    assert_response :success
  end

  test "should create api_v1_data_set" do
    assert_difference('Api::V1::DataSet.count') do
      post api_v1_data_sets_url, params: { api_v1_data_set: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show api_v1_data_set" do
    get api_v1_data_set_url(@api_v1_data_set), as: :json
    assert_response :success
  end

  test "should update api_v1_data_set" do
    patch api_v1_data_set_url(@api_v1_data_set), params: { api_v1_data_set: {  } }, as: :json
    assert_response 200
  end

  test "should destroy api_v1_data_set" do
    assert_difference('Api::V1::DataSet.count', -1) do
      delete api_v1_data_set_url(@api_v1_data_set), as: :json
    end

    assert_response 204
  end
end
