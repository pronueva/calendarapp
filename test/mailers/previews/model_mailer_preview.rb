# Preview all emails at http://localhost:3000/rails/mailers/model_mailer
class ModelMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/model_mailer/send_calendar_employee
  def send_calendar_employee
    ModelMailer.send_calendar_employee
  end

end
