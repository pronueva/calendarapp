require 'test_helper'

class ModelMailerTest < ActionMailer::TestCase
  test "send_calendar_employee" do
    mail = ModelMailer.send_calendar_employee
    assert_equal "Send calendar employee", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
