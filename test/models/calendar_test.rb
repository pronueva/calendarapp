# == Schema Information
#
# Table name: calendars
#
#  id          :integer          not null, primary key
#  user_id     :integer          not null
#  employee_id :integer          not null
#  shift_id    :integer          not null
#  date        :date
#  published   :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  code        :string
#
# Indexes
#
#  index_calendars_on_employee_id  (employee_id)
#  index_calendars_on_shift_id     (shift_id)
#  index_calendars_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (shift_id => shifts.id)
#  fk_rails_...  (user_id => users.id)
#

require 'test_helper'

class CalendarTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
