# == Schema Information
#
# Table name: employees
#
#  id          :integer          not null, primary key
#  first_name  :string           default(""), not null
#  last_name   :string           default(""), not null
#  rut         :string(13)       default(""), not null
#  email       :string           default(""), not null
#  tel         :string           default(""), not null
#  address_id  :integer
#  contract_id :integer
#  benefit_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  area_id     :integer
#
# Indexes
#
#  index_employees_on_area_id  (area_id)
#
# Foreign Keys
#
#  fk_rails_...  (area_id => areas.id)
#  fk_rails_...  (benefit_id => benefits.id)
#  fk_rails_...  (contract_id => contracts.id)
#

require 'test_helper'

class EmployeeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
