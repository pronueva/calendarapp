window.App or= {};
window.ENV or= {};
window.ENV.API_HOST = "https://calendarapp-dev.herokuapp.com/";
window.App.token = document.querySelector('meta[name="csrf-token"').content;
window.ENV.API_URL = window.ENV.API_HOST+"/api/v1/";