json.extract! restriction, :id, :created_at, :updated_at
json.url restriction_url(restriction, format: :json)
