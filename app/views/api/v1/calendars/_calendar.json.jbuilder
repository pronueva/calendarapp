json.extract! calendar, :id, :user_id, :employee_id, :shift_id, :date, :published, :created_at, :updated_at
json.url api_v1_calendars_url(calendar, format: :json)
