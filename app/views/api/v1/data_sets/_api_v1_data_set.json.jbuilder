json.extract! api_v1_data_set, :id, :created_at, :updated_at
json.url api_v1_data_set_url(api_v1_data_set, format: :json)
