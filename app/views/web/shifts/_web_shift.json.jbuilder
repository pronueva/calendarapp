json.extract! web_shift, :id, :created_at, :updated_at
json.url web_shift_url(web_shift, format: :json)
