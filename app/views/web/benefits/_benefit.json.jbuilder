json.extract! benefit, :id, :created_at, :updated_at
json.url benefit_url(benefit, format: :json)
