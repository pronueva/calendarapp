json.extract! web_department, :id, :created_at, :updated_at
json.url web_department_url(web_department, format: :json)
