class Api::V1::ContractsController < ApplicationController
  def index
    @contracts = Contract.get_contracts.select(:id, :contract_type, :description)
    render json: @contracts
  end
end
