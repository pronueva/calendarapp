class Api::V1::CalendarLogsController < ApplicationController
  before_action :set_logs, only: [:show]
  before_action :set_dep, only:[:logs_dep]
  before_action :set_date, only:[:logs_date]

  def index
    render json: @calendar_logs = CalendarLog.select(:id, :department_id, :code, :published).order(:id).reverse_order
  end

  def show
    @calendar_log = CalendarLog.where(code: params[:code])
    if @calendar_log
      render json: @calendar_log
    else
      render json: {warning: 'ID Log de asignación no encontrado'}, status: :not_found
    end
  end

  def logs_dep
    if @calendar_logs.any?
      render json: @calendar_logs
    else
      render json: {warning: 'No existe registro Log de asignación para el departamento'}, status: :not_found
    end
  end

  def logs_date
    if @calendar_logs.any?
      render json: @calendar_logs
    else
      render json: {warning: 'No existe registro Log de asignación para la fecha'}, status: :not_found
    end
  end

  private
  def set_logs
    @calendar_log = CalendarLog.find_by_id(params[:code])
  end

  def set_dep
    @calendar_logs = CalendarLog.where(department_id: params[:department_id])
  end

  def set_date
    @calendar_logs = CalendarLog.where(date: params[:date])
  end
end
