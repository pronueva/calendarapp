class Api::V1::BenefitsController < ApplicationController
  before_action :set_benefit, only: [:index, :show]

  def index
    render json: @benefits = Benefit.all
  end

  def show
    if @benefit
      render json: @benefit
    else
      render json: {error: 'Beneficio no encontrado.', status: not_found}
    end
  end

  private
    def set_benefit
      @benefit = Benefit.find_by_id(params[:id])
    end

end
