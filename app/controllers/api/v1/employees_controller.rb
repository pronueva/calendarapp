class Api::V1::EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :update, :destroy]

  def index
    #@employees = Employee.all
    @employees = Employee.select('employees.id,
                                  employees.department_id,
                                  employees.first_name,
                                  employees.last_name,
                                  employees.department_id,
                                  contracts.contract_type,
                                  benefits.cod,
                                  benefits.description').joins('INNER JOIN contracts
                                                        ON employees.contract_id = contracts.id
                                                        INNER JOIN benefits
                                                        ON contracts.benefit_id = benefits.id')
    render json: @employees
  end

  def show
    if @employee
      render json: @employee
    else
      render json: {error: 'Empleado no encontrado'}, status: :not_found
    end
  end

  def create
    @employee = Employee.new(employee_params)

    if @employee.save
      render :show, status: :created
    else
      render json: @employee.errors, status: :unprocessable_entity
    end
  end

  def update
    if @employee.update(employee_params)
      render :show, status: :ok
    else
      render json: @employee.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @employee.destroy
    render json: {result: 'Empleado elimnado correctamente'}, status: :no_content
  end

  private

    def set_employee
      @employee = Employee.find_by_id(params[:id])
    end

    def employee_params
      params.require(:employee).permit(:first_name, :last_name, :rut, :email, :tel)
    end
end
