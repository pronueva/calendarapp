class Api::V1::RestrictionsController < ApplicationController
  before_action :set_restriction, only: [:show, :update, :destroy]

  def index
    render json: @restrictions = Restriction.all
  end

  def show
    if @restriction
      render json: @restriction
    else
      render json: {error: 'Restricción no encontrada'}, status: :not_found
    end
  end

  def create
    @restriction = Restriction.new(restriction_params)

    if @restriction.save
      render :show, status: :created
    else
      render json: @restriction.errors, status: :unprocessable_entity
    end
  end

  def update
    if @restriction.update(restriction_params)
      render :show, status: :ok
    else
      render json: @restriction.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @restriction.destroy
    render json: {result: 'Restricción eliminada'}, status: :no_content
  end

  private

    def set_restriction
      @restriction = Restriction.find_by_id(params[:id])
    end

    def restriction_params
      params.require(:restriction).permit(:cod, :name, :restrict_type, :description)
    end
end
