class Api::V1::ShiftsController < ApplicationController
  before_action :set_shift, only: [:show, :edit, :update, :destroy]

  def index
    render json: @shifts = Shift.all
  end

  def show
    if @shift
      render json: @shift
    else
      render json: {error: 'Turno no encontrado'}, status: :not_found
    end
  end

  def create
    @shift = Shift.new(shift_params)
    if @shift.save
      render :show, status: :created, location: @shift
    else
      render json: @shift.errors, status: :unprocessable_entity
    end
  end

  def update
    if @shift.update(shift_params)
      render json: :show, status: :ok
    else
      render json: @shift.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @shift.destroy
    render json: {result: 'Turno elimnado correctamente'}, status: :no_content
  end

  private

    def set_shift
      @shift = Shift.find_by_id(params[:id])
    end

    def shift_params
      params.require(:shift).permit(:cod, :shift_start, :shift_end, :break_start, :break_end, :shift_type, :description)
    end
end
