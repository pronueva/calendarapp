class Api::V1::CalendarsController < ApplicationController
  before_action :set_calendar, only: [:show, :update, :destroy]
  before_action :set_calendar_code, only: [:calendar_code]

  def index
    render json: @calendars = Calendar.all.order(:id).reverse_order
  end

  def calendar_code
    if @calendars.any?
      render json: @calendars
    else
      render json: {error: 'No se encontró codigo asignado en calendario'}, status: :not_found
    end
  end

  def show
    if @calendar
      render json: @calendar
    else
      render json: {error: 'Asignación de calendario no encontrado'}, status: :not_found
    end
  end

  def create
    @calendar_log = CalendarLog.find_by(:code => calendar_params["code"])
    #create calendar_logs if not exists ~>
    if @calendar_log.nil?
      @calendar_logs = CalendarLog.new
      @calendar_logs.code =  calendar_params["code"]
      @calendar_logs.published =  0
      @calendar_logs.date =  Time.new( calendar_params["date"]["currentYear"], calendar_params["date"]["currentMonth"] + 1, 1 )
      @calendar_logs.area_id =  calendar_params["area"]
    else
      @calendar_logs = @calendar_log
    end
    @count = 0
    @errs = 0
    #check if calendar is not completed
    calendar_params["employees"].each do |e|

      @allocations = e["allocations"]
      @employee = e["id"]
      @allocations.each do |a|
        if a["shift"].nil?
          render json: "Dia " + a["day_number"].to_s + "/" + (a["month"] + 1).to_s + " Empleado " + Employee.find(e["id"]).first_name + " " + Employee.find(e["id"]).last_name + " debe ingresar turno"
          return false
        else
          #check and count calendar for errors/alerts ~>
          if a["status"] > 1
            @errs = @errs + 1
          end
        end
      end
      #save calendar_log ~>
      @calendar_logs.count_errors = @errs
      @calendar_logs.save!
      #save calendar allocations ~>
      @calendar_data = Calendar.where("employee_id = #{@employee} AND code = '#{@calendar_logs.code}'")

      @allocations.each_with_index do |a,index|

        if !@calendar_data[index].nil?
          @calendar =  Calendar.find(@calendar_data[index].id)
        else
          @calendar =  Calendar.new
        end
        @calendar.employee_id =  e["id"]
        @calendar.code =  calendar_params["code"]
        @calendar.user_id =  current_user.id
        @calendar.published =  0
        date = Time.new( calendar_params["date"]["currentYear"], a["month"] + 1, a["day_number"] )
        @calendar.date =  date
        @calendar.shift_id =  a["shift"]
        @calendar.save!
        @count = @count + 1

      end
    end
    #render json: @calendar
  end

  def update
    if @calendar.update(calendar_params)
      render :show, status: :ok
    else
      render json: @calendar.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @calendar.destroy
      render json: {result: 'Asignación de turno eliminado correctamente'}, status: :no_content
    else
      render json: @calendar.errors, status: :unprocessable_entity
    end
  end

  private

    def set_calendar
      @calendar = Calendar.find_by_id(params[:id])
    end

    def set_calendar_code
      @calendars = Calendar.all.where(code: params[:code])
    end

    def calendar_params
      params.require(:calendar)
    end
end
