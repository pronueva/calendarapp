class Api::V1::HolidaysController < ApplicationController
  before_action :set_holidays_year, only: :list_year
  before_action :set_holidays_month_year, only: :list_month_year
  def index
    @holidays = Holiday.all()
    render json: @holidays
  end

  def list_year
    if @calendars.any?
      render json: @holidays
    else
      render json: {error: 'Sin registro de feriados para el año solicitado'}
    end
  end

  def list_month_year
    if @holidays.any?
      render json: @holidays
    else
      render json: {error: 'Mes sin feriados'}, status: :not_found
    end
  end
  private
  def set_holidays_year
    @holidays = Holiday.where('extract(year from date) = ?', params[:year])
  end

  def set_holidays_month_year
    @holidays = Holiday.where('extract(month from date) = ?', params[:month]).where('extract(year from date) = ?', params[:year])
  end
end
