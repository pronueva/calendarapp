class Api::V1::ListShiftController < ApplicationController
  before_action :set_shift_type, only: :search_shift_type
  def index
    render json: @shifts = Shift.all.order(id: :asc)
  end

  def search_shift_type
    if @shifts
      render json: @shifts
    else
      render json: {error: 'Tipo de turno no encontrado'}, status: :not_found
    end
  end
  private
  def set_shift_type
    @shifts = Shift.where(time: params[:time])
  end

end
