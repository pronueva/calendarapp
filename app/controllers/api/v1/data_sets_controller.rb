class Api::V1::DataSetsController < ApplicationController
  # skip_before_action :verify_authenticity_token
  # before_action :authenticate!, only: [:index, :show]
  before_action :set_data, only: [:show]

  def index
    render json: @area = Area.all
  end
  def departments
    @departments = Department.get_all_departments_with_areas
    render json: @departments.to_json(:include => {:areas => {:only => [:id, :name, :description]}})
      # render json: @departments.to_json(:include => :areas)
  end

  def show
    if @area
      render :json => @area.to_json( :include => {:employees => {:only => [:id, :first_name, :last_name, :email, :contract_id, :benefit_id ]}})
    else
      render json: {error: 'Área no encontrado'}, status: :not_found
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_data
      @area = Area.find_by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def area_set_params
      params.require.(:area).permit(:name, :department, :branch_id)
    end
end
