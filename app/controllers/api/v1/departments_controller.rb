class Api::V1::DepartmentsController < ApplicationController
  before_action :set_department, only: [:show, :update, :destroy]

  def index
    render json: @departments = Department.all
  end

  def show
    if @department
      #render :json => @department.to_json( :include =>  [:employees])
=begin
      @department = Employee.select('employees.id,
                                  employees.department_id,
                                  employees.first_name,
                                  employees.last_name,
                                  employees.department_id,
                                  contracts.contract_type,
                                  benefits.cod,
                                  benefits.description').joins('INNER JOIN contracts
                                                        ON employees.contract_id = contracts.id
                                                        INNER JOIN benefits
                                                        ON contracts.benefit_id = benefits.id').where(department_id: params[:id])
=end
      @department = Employee.select('employees.id,
                                    employees.first_name,
                                    employees.last_name,
                                    employees.contract_id,
                                    employees.benefit_id,
                                    benefits.cod,
                                    benefits.description').joins('INNER JOIN contracts
                                                            on employees.contract_id = contracts.id
                                                            INNER JOIN benefits
                                                            on employees.benefit_id = benefits.id').where(department_id: params[:id])
      render json: @department
    else
      render json: {error: 'Departamento no encontrado'}, status: :not_found
    end
  end

  def create
    @department = Department.new(department_params)

    if @department.save
      render :show, status: :created
    else
      render json: @department.errors, status: :unprocessable_entity
    end
  end

  def update
    if @department.update(department_params)
      render :show, status: :ok
    else
      render json: @department.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @department.destroy
    render json: {result: 'Departamento eliminado correctamente'}, status: :no_content
  end

  private

    def set_department
      @department = Department.find_by_id(params[:id])
    end

    def department_params
      params.require.(:department).permit(:name, :area, :branch_id)
    end
end
