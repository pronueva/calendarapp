class MainController < ApplicationController
  add_breadcrumb 'Home', :root_path
  def home
    @path = "Dashboard"
  end
  def calendarApp
    add_breadcrumb "Asignaciones", '/calendarApp'
  end
end
