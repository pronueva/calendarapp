class Web::DepartmentsController < ApplicationController
  before_action :set_department, only: [:show, :edit, :update, :destroy]
  before_action :validate_user
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Departamentos", :web_departments_path

  def index
    @departments =Department.all.order(:id).includes(:areas)
  end

  def show
    add_breadcrumb @department.name, web_department_path
  end

  def new
    @department = Department.new
    # add_breadcrumb "Crear departamento", new_web_department_path
  end

  def edit
  end

  def create
    @department = Department.create(department_params)
    if @department.save
      flash[:notice] = 'Departamento creado'
    else
      render 'new'
    end
  end

  def update
    if @department.update(department_params)
      flash[:notice] = 'Departamento Actualizado'
      redirect_to action: :index
    else
      render 'edit'
    end
  end

  # DELETE /web/departments/1
  # DELETE /web/departments/1.json
  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to web_departments_url, notice: 'Department was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_params
      params.require(:department).permit(:name, :description )
    end

    def validate_user
      if current_user.nil? || current_user.operator?
        redirect_to new_user_session_path, notice: 'No Tiene permisos para esta seccion'
      end
    end
end
