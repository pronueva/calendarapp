class Web::UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :validate_user
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Usuarios", :web_users_path
  def index
    @users = User.current(current_user).order(:id)
  end

  def show
    add_breadcrumb "Usuario #{@user.first_name}", web_user_path
  end

  def new
    @user = User.new
    add_breadcrumb "Nuevo Usuario", new_web_user_path
  end

  def edit
    add_breadcrumb "Edición #{@user.first_name}"
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = 'Usuario creado'
      ModelMailer.send_email(@user, "Se le ha invitado al sistema Waki").deliver_now
      redirect_to action: :index
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      ModelMailer.change_account(@user).deliver_now
      flash[:notice] = 'Usuario de sistema actualizado'
      redirect_to action: :index
    else
      render :update
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to web_users_url, notice: 'Users was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
  def set_user
    @user = User.find_by_id(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :rut, :email, :role)
  end
  def validate_user
    if current_user.nil? || current_user.admin? || current_user.operator?
      redirect_to new_user_session_path, notice: 'No Tiene permisos para esta seccion'
    end
  end

end
