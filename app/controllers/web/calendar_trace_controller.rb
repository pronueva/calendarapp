class Web::CalendarTraceController < ApplicationController
  add_breadcrumb "Home", :root_path

  def index
    add_breadcrumb "Seguimiento de asignaciones"
    @calendars = Calendar.current_week
    @employees = Employee.select(:id, :first_name, :last_name).includes(:calendars)
    @week = Calendar.select(:date).order(:date).distinct(:date).where('date >= ? and date <= ?', Date.today.beginning_of_week.strftime("%Y-%m-%d"),Date.today.at_end_of_week.strftime("%Y-%m-%d"))
  end

  def date
    @calendars = Calendar.last(7).includes(:shift)
    @employees.each do |e|
      e.calendars.last(7).each do |emca|
        @calendars.each do |cal|
          if emca.date = cal.date
            @test << emca
          end
        end
      end
    end
  end
  def real_status
    @date = Time.now
  end

  def assistance
    word = params[:keyword]
    Rails.logger.info "PARAMS: " + params.inspect + " ////////////////"
    if word
      @employees = Calendar.get_shifts_employees_per_month(word)
      Rails.logger.info "PARAMS @EMPLOYEE" + @employees.inspect
    else
      @employees = []
    end
    respond_to do |format|
      format.js
    end
  end

end





