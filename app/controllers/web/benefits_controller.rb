class Web::BenefitsController < ApplicationController
  before_action :set_benefit, only: [:show, :edit, :update, :destroy]
  before_action :validate_user
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Domingos Libres y otros beneficios", :web_benefits_path

  def index
    @benefits = Benefit.all.order(:id)
  end

  def show
    add_breadcrumb @benefit.cod, web_benefit_path
  end

  # GET /benefits/new
  def new
    @benefit = Benefit.new
    add_breadcrumb "Nuevo Beneficio", new_web_benefit_path
  end

  def edit
    add_breadcrumb "Edición #{@benefit.cod}"
  end

  def create
    @benefit = Benefit.new(benefit_params)
    if @benefit.save
      flash[:notice] = 'Beneficio creado correctamente'
      redirect_to action: :index
    else
      render 'new'
    end
  end

  def update
    if @benefit.update(benefit_params)
      redirect_to action: :index
      flash[:notice] = 'El Beneficio ha sido actualizado.'
    else
      render 'update'
    end
  end

  def destroy
    if @benefit.destroy!
      redirect_to action: :index
      flash[:notice] = 'El Beneficio ha sido Eliminado.'
    else
      render 'index'
      flash[:notice] = 'El Beneficio no se puede eliminar. Actualmente está asignado a uno o varios empleados'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_benefit
      @benefit = Benefit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def benefit_params
      params.require(:benefit).permit(:cod, :description)
    end

    def validate_user
      if current_user.nil? || current_user.operator?
        redirect_to new_user_session_path, notice: 'No Tiene permisos para esta seccion'
      end
    end
end
