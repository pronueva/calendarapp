class Web::ShiftsController < ApplicationController
  before_action :set_shift, only: [:show, :edit, :update, :destroy]
  before_action :validate_user, except: :index
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Turnos", :web_shifts_path
  def index
    @shifts = Shift.all.order(:id).paginate(:per_page => 10, :page => params[:page])
  end

  def show
    add_breadcrumb @shift.cod, web_shift_path
  end

  def new
    @shift = Shift.new
    add_breadcrumb "Nuevo turno", new_web_shift_path
  end

  def edit
    add_breadcrumb "Edición #{@shift.cod}", edit_web_shift_path
  end

  def create
    @shift = Shift.new(shift_params)
    if @shift.save
      flash[:notice] = 'El Turno ha sido creado.'
      redirect_to action: :index
    else
      render :new
    end
  end

  def update
    if @shift.update(shift_params)
      flash[:notice] = 'Turno modificado correctamente.'
      redirect_to action: :index
    else
      render :update
    end
  end

  def destroy
    @shift.destroy
    respond_to do |format|
      format.html { redirect_to web_shifts_url, notice: 'Shift was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shift
      @shift = Shift.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shift_params
      params.require(:shift).permit(:cod, :shift_start, :shift_end, :break_start, :break_end, :shift_type, :description, :time, :per_hour )
    end

    def validate_user
      if current_user.nil? || current_user.operator?
        redirect_to new_user_session_path, notice: 'No Tiene permisos para esta seccion'
      end
    end
end
