class Web::EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  before_action :validate_user, only: [:new, :edit, :update, :destroy]
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Departamentos", :web_departments_path
  add_breadcrumb "Empleados", :web_employees_path

  def index
    @employees = Employee.all.order(:id).includes(:area, :contract ,:benefit)
  end

  def show
    add_breadcrumb "#{@employee.first_name} #{@employee.last_name}", web_employee_path
  end

  def new
    @employee = Employee.includes(:areas).new
    add_breadcrumb "Nuevo empleado", new_web_employee_path
  end

  def edit
    add_breadcrumb "Edición #{@employee.first_name} #{@employee.last_name}", edit_web_employee_path
  end

  def create
    @employee = Employee.new(employee_params)
    if @employee.save
      flash[:notice] = 'El empleado ha sido creado.'
      redirect_to action: :index
    else
      render 'new'
    end
  end

  def update
    if @employee.update(employee_params)
      flash[:notice] = 'El Emplado ha sido actualizado'
      redirect_to action: :index
    else
      render 'edit'
    end
  end

  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to web_employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:first_name, :last_name, :rut, :email, :tel,:area_id, :address_id, :contract_id, :benefit_id)
    end

    def validate_user
      if current_user.nil? || current_user.operator?
        redirect_to new_user_session_path, notice: 'No Tiene permisos para esta seccion'
      end
    end

end
