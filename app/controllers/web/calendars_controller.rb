class Web::CalendarsController < ApplicationController
  before_action :set_calendar, only: [:show, :edit, :update, :destroy ]
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Programación mensual", :web_calendars_path
  def index
    @calendars = Calendar.all
    require 'date'
    @date = Date.today.strftime( '%b %Y')
    @present_month = Calendar.get_date_calendar_to_the_specific_month(Time.now.month)
    @employees = Employee.select(:id, :first_name, :last_name)
    respond_to do  |format|
      format.html
      format.pdf { render template: 'web/calendars/reports/reporte', pdf: 'horario', layout: 'pdf.html', page_size: 'A4', dpi: '210', orientation: 'Landscape' }
    end
  end

  def select_calendar
    date = params[:keyword]
    @present_month = Calendar.get_date_calendar_to_the_specific_month(date)
    @employees = Employee.select(:id, :first_name, :last_name)
    respond_to do  |format|
      format.html
      format.pdf { render template: 'web/calendars/reports/reporte', pdf: 'horario', layout: 'pdf.html', page_size: 'A4', dpi: '210', orientation: 'Landscape' }
    end
  end

  def update
    @calendar.update(calendars_params)
  end

  private
  def set_calendar
  @calendar = Calendar.find(params[:id])
  end
  def calendars_params
    params.require(:calendar).permit(:user_id, :employee_id, :shift_id, :date, :published, :code)
  end
end
