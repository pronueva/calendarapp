class Web::ContractsController < ApplicationController
  before_action :set_contract, only: [:show, :edit, :update, :destroy]
  before_action :validate_user
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Contratos", :web_contracts_path

  def index
    @contracts = Contract.all.order(:id)
  end

  def show
    add_breadcrumb "Contrato #{@contract.contract_type}", web_contract_path
  end

  def new
    @contract = Contract.new
    add_breadcrumb "Nuevo contrato", new_web_contract_path
  end

  def edit
    add_breadcrumb "Edición contrato #{@contract.contract_type}", edit_web_contract_path
  end

  def create
    @contract = Contract.new(contract_params)
    if @contract.save
      Rails.logger.info "CONTRATO CREADO" + @contract.inspect + " ////////////"
      flash[:notice] = 'El Contrato ha sido creado.'
      redirect_to action: :index
    else
      Rails.logger.info "CONTRATO NO CREADO" + @contract.inspect + " ////////////"
      render 'new'
    end
  end

  def update
    if @contract.update(contract_params)
      flash[:notice] = 'Contrato modificado correctamente'
      redirect_to action: :index
    else
      render 'edit'
    end
  end

  def destroy
    if @contract.destroy!
      redirect_to action: :index
      flash[:notice] = 'El Contrato ha sido Eliminado.'
    else
      render 'index'
      flash[:notice] = 'El contrato no se puede eliminar, actualmente está asignado a uno o varios empleados'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contract
      @contract = Contract.find_by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contract_params
      params.require(:contract).permit(:contract_type, :description)
    end

    def validate_user
      if current_user.nil? || current_user.operator?
        redirect_to new_user_session_path, notice: 'No Tiene permisos para esta seccion'
      end
    end
end
