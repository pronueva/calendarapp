class Web::HolidaysController < ApplicationController
  before_action :validate_user
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Feriados", :web_holidays_path
  def index
    @holidays = Holiday.all
  end

  def new
    @holiday = Holiday.new
  end

  def refill
    @source = JSON.parse(HTTParty.get('https://www.feriadosapp.com/api/holidays.json').body)
    @source['data'].each do |var|
      @holiday = Holiday.create(
          :date => var['date'],
          :name => var['title'],
          :description => var['extra']
      ) unless Holiday.where(date: var['date']).first
    end
    flash[:notice] = 'Feriados actualizados'
    redirect_to action: :index
  end

  private
    def holiday_params
      params.require(:holiday).permit(:date, :name, :description)
    end

    def validate_user
      if current_user.nil? || current_user.operator?
        redirect_to new_user_session_path, notice: 'No Tiene permisos para esta seccion'
      end
    end
end
