class Web::AreasController < ApplicationController
  before_action :set_area, only: [:show, :edit, :update, :destroy]
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Departamentos", :web_departments_path
  add_breadcrumb "Áreas"

  def index
    @areas = Area.all
  end

  def new
    @area = Area.new
  end

  def show
    add_breadcrumb @area.name, web_area_path
  end

  def edit
  end

  def create
    @area = Area.create(area_params)
    if @area.save
      flash[:notice] = 'Area creada'
      redirect_to web_departments_path
    else
      render 'new'
    end
  end

  def update
    if @area.update(area_params)
      flash[:notice] = 'Departamento Actualizado'
      redirect_to web_departments_path
    else
      render 'edit'
    end
  end

  def destroy
    @area.destroy
    respond_to do |format|
      format.html { redirect_to web_departments_url, notice: 'Area was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  def set_area
    @area = Area.find_by_id(params[:id])
  end

  def area_params
    params.require(:area).permit(:name, :description, :department_id)
  end
end