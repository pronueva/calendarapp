module V2
  class Shifts < Grape::API
    resources :shifts do
      desc 'Get all shifts'
      get do
        Shift.all
      end
    end
  end
end