module V2
  class Root < Grape::API
    version 'v2'
    format :json
    prefix :api
    get :status do
      {status: 'OK!'}
    end

    mount V2::Shifts
    mount V2::Departments

  end
end
