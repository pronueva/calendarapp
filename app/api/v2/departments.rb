module V2
  class Departments < Grape::API
    resources :departments do
      desc 'return all departments'
      get do
        Department.all
      end

      desc 'return department with employees'
      get do
        Department.find(params[:id]).include.employees
      end
    end
  end
end