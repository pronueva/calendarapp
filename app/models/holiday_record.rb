# == Schema Information
#
# Table name: holiday_records
#
#  id          :integer          not null, primary key
#  holiday_id  :integer
#  employee_id :integer
#  status      :boolean
#  offset      :date
#
# Indexes
#
#  index_holiday_records_on_employee_id  (employee_id)
#  index_holiday_records_on_holiday_id   (holiday_id)
#
# Foreign Keys
#
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (holiday_id => holidays.id)
#

class HolidayRecord < ApplicationRecord
  belongs_to :holiday
  belongs_to :employee
end
