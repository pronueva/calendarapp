# == Schema Information
#
# Table name: restrictions
#
#  id            :integer          not null, primary key
#  cod           :string           default(""), not null
#  name          :string           default(""), not null
#  restrict_type :string           default(""), not null
#  description   :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Restriction < ApplicationRecord
  has_and_belongs_to_many :calendars
end
