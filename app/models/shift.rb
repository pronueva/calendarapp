# == Schema Information
#
# Table name: shifts
#
#  id          :integer          not null, primary key
#  cod         :string           default(""), not null
#  shift_start :time             default(Sat, 01 Jan 2000 00:00:00 UTC +00:00), not null
#  shift_end   :time             default(Sat, 01 Jan 2000 00:00:00 UTC +00:00), not null
#  break_start :time             default(Sat, 01 Jan 2000 00:00:00 UTC +00:00), not null
#  break_end   :time             default(Sat, 01 Jan 2000 00:00:00 UTC +00:00), not null
#  shift_type  :string           default(""), not null
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  time        :integer          default("am")
#  per_hour    :integer
#

class Shift < ApplicationRecord
  has_many :calendars
  has_many :employees, :through => :calendars
  validates :cod, presence: {:message => "Es necesario asignar un código al turno"}
  validates :description, presence: {:message => "Añada una descripción"}
  enum time: [:intermedio, :am, :pm, :free]
end
