# == Schema Information
#
# Table name: contracts
#
#  id            :integer          not null, primary key
#  contract_type :string
#  description   :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Contract < ApplicationRecord
  has_many :employees
  validates :contract_type, presence: {:message => "Se requiere un nombre para el contrato."}
  validates :description, presence: {:message => "Se requiere el contenido del contrato."}, length: {minimum: 20, :message => "El texto del contrato es demasaido corto."}

  scope :get_contracts, -> {Contract.all}

end
