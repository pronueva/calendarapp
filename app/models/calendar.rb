# == Schema Information
#
# Table name: calendars
#
#  id          :integer          not null, primary key
#  user_id     :integer          not null
#  employee_id :integer          not null
#  shift_id    :integer          not null
#  date        :date
#  published   :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  code        :string
#
# Indexes
#
#  index_calendars_on_employee_id  (employee_id)
#  index_calendars_on_shift_id     (shift_id)
#  index_calendars_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (employee_id => employees.id)
#  fk_rails_...  (shift_id => shifts.id)
#  fk_rails_...  (user_id => users.id)
#

class Calendar < ApplicationRecord
  has_and_belongs_to_many :restrictions
  belongs_to :user
  belongs_to :employee
  belongs_to :shift

  # web/calendar_trace_controller
  scope :get_shifts_employees_per_month, -> (id_employee) { joins(:employee, :shift).where('extract(month from calendars.date) = ? AND employees.id = ?', 2, id_employee).select(:cod).group(:cod).count(:cod) }
  # web/calendar
  scope :get_date_calendar_to_the_specific_month, -> (month) { select(:date).distinct.where('extract(month from calendars.date) = ?', month) }
  scope :current_month, -> { select('calendars.*, extract(month from calendars.date)').where('extract(month from calendars.date) = ?', Time.now.month) }
  scope :current_week, -> { select(:id, :date, :shift_id).order(:date).distinct(:date).where('date >= ? and date <= ?', Date.today.beginning_of_week.strftime("%Y-%m-%d"),Date.today.at_end_of_week.strftime("%Y-%m-%d")) }
end
