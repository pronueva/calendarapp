# == Schema Information
#
# Table name: benefits
#
#  id          :integer          not null, primary key
#  cod         :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Benefit < ApplicationRecord
  has_many :employees
  validates :cod, presence: {:message => "Se requiere un código para el Beneficio."}
  validates :description, presence: {:message => "Se requiere una descripción del contrato."}, length: {minimum: 10, :message => "La descripción es demasaido corta."}
end
