# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string           default(""), not null
#  last_name              :string           default(""), not null
#  rut                    :string(13)       default(""), not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  role                   :integer
#  branch_office_id       :integer
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  api_key                :string
#
# Indexes
#
#  index_users_on_branch_office_id      (branch_office_id)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_role                  (role)
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  enum role: [:root, :admin, :operator]
  has_many :calendars

  # create new users from root session
  after_create {|user| user.send_reset_password_instructions }

  def password_required?
    new_record? ? false:super
  end

  # Assign an API key on create
  before_create do |user|
    user.api_key = user.generate_api_key
  end
  # Generate an unique API key
  def generate_api_key
    loop do
      token = Devise.friendly_token
      break token unless User.where(api_key: token).first
    end
  end
  # find all users except current user
  def self.current(user)
    where.not(id: user)
  end

end
