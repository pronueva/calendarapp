# == Schema Information
#
# Table name: areas
#
#  id            :integer          not null, primary key
#  name          :string
#  description   :text
#  department_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_areas_on_department_id  (department_id)
#
# Foreign Keys
#
#  fk_rails_...  (department_id => departments.id)
#

class Area < ApplicationRecord
  belongs_to :department
  has_many :employees, -> {order(:id => :asc)}
end
