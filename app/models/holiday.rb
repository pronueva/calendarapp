# == Schema Information
#
# Table name: holidays
#
#  id          :integer          not null, primary key
#  date        :date
#  name        :string
#  description :text
#

class Holiday < ApplicationRecord
  has_many :holiday_record
end
