# == Schema Information
#
# Table name: employees
#
#  id          :integer          not null, primary key
#  first_name  :string           default(""), not null
#  last_name   :string           default(""), not null
#  rut         :string(13)       default(""), not null
#  email       :string           default(""), not null
#  tel         :string           default(""), not null
#  address_id  :integer
#  contract_id :integer
#  benefit_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  area_id     :integer
#
# Indexes
#
#  index_employees_on_area_id  (area_id)
#
# Foreign Keys
#
#  fk_rails_...  (area_id => areas.id)
#  fk_rails_...  (benefit_id => benefits.id)
#  fk_rails_...  (contract_id => contracts.id)
#

class Employee < ApplicationRecord
  validates :first_name, presence: {:message => "Ingrese Nombre del Empleado"}
  validates :last_name, presence: {:message => "Ingrese Apellidos del Empleado"}
  validates :rut, presence:{:message => "Es necesario el Rut del Empleado"}
  validates :tel, presence:{:message => "Se requiere número de teléfono"}
  has_one :department, :through => :area
  belongs_to :area
  belongs_to :contract
  belongs_to :benefit
  has_many :calendars
  has_many :shifts, :through => :calendars
  has_many :holiday_records

  # get full name employee
  def full_name; "#{first_name} #{last_name}"; end

end
