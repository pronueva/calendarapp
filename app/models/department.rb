# == Schema Information
#
# Table name: departments
#
#  id           :integer          not null, primary key
#  name         :string           default(""), not null
#  description  :string           default(""), not null
#  id_branch_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_departments_on_id_branch_id  (id_branch_id)
#

class Department < ApplicationRecord
  validates :name, presence: {:message => "Se requiere nombre al Departamento"}
  validates_uniqueness_of :name, {:message => "Ya existe un departamento con ese nombre"}
  has_many :areas, dependent: :destroy
  has_many :calendar_logs

  scope :get_all_departments_with_areas, -> {Department.select(:id, :name, :description)}
end
