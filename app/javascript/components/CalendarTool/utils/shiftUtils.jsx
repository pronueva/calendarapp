/**
 * Devuelve el turno seleccionado
 * @params shifts , value
 * return Object
 */
export function getSelectedShift(shifts,value){
    return shifts.filter( function(s){
        if(s.id === value){
            return s
        }
    })[0];
}