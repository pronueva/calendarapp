/**
 * Validar domingos por contrato, valida que existe un cambio en los domingos segun beneficio
 * @params employee , week , sunday_data , freeShiftArr
 * return data Object
 */
export function benefitSundaysValdiation( employee  , week , sunday_data , freeShiftArr){
    if(employee.benefit.cod === "DL1"){
        if(week.id === 1 || week.id === 2){
            //CAMBIO EN DOMINGOS LIBRES POR CONTRATO
            if(sunday_data.shift !== 1){
                sunday_data.err_msg = "Se ha cambiado D/L";
                sunday_data.status = 3; //warning
                sunday_data.worked_day = 1;
            }else{
                sunday_data.err_msg = "Domingo libre por contrato";
                sunday_data.status = 1; //success
                sunday_data.worked_day = 0;
            }
        }
    }
    if(employee.benefit.cod === "DL2"){
        if(week.id === 2 || week.id === 3){
            //CAMBIO EN DOMINGOS LIBRES POR CONTRATO
            if(sunday_data.shift !== 1){
                sunday_data.err_msg = "Se ha cambiado D/L";
                sunday_data.status = 3; //err
                sunday_data.worked_day = 1;
            }else{
                sunday_data.err_msg = "Domingo libre por contrato";
                sunday_data.status = 1; //warnsuccessing
                sunday_data.worked_day = 0;
            }
        }
    }
    if(employee.benefit.cod === "DL3"){
        if(week.id === 3 || week.id === 4){
            //CAMBIO EN DOMINGOS LIBRES POR CONTRATO
            if(sunday_data.shift !== 1){
                sunday_data.err_msg = "Se ha cambiado D/L";
                sunday_data.status = 3; //err
                sunday_data.worked_day = 1;
            }else{
                sunday_data.err_msg = "Domingo libre por contrato";
                sunday_data.status = 1; //warnsuccessing
                sunday_data.worked_day = 0;
            }
        }
    }
    if(employee.benefit.cod === "DL4"){
        if(week.id === 3 || week.id === 4){
            //CAMBIO EN DOMINGOS LIBRES POR CONTRATO
            if(sunday_data.shift !== 1){
                sunday_data.err_msg = "Se ha cambiado D/L";
                sunday_data.status = 3; //err
                sunday_data.worked_day = 1;
            }else{
                sunday_data.err_msg = "Domingo libre por contrato";
                sunday_data.status = 1; //warnsuccessing
                sunday_data.worked_day = 0;
            }
        }
    }
    return sunday_data;
}
/**
 * -----------------------
 * VALIDACION 2 DOMINGOS LIBRES SEGUIDOS MAXIMO
 * ALERTA DE PRODUCTIVIDAD (AMARILLO)
 * -----------------------
 * @params weeks , nextProps , freeShiftArr
 * return data array
 **/
export function maxTwoFreeSundays(weeks, nextProps){
    let new_data = [];
    weeks.splice(0,1);
    weeks.map( (w) => {
        let key = w.key*7;
        new_data[key] = {};;
        new_data[key].status = 3
        new_data[key].employee = nextProps.employee.id;
        new_data[key].day_index = key;
        new_data[key].err_msg = "2+ Domingos libres";
        new_data[key].shift = nextProps.assignment.employees[nextProps.employee.id].allocations[key].shift;
        new_data[key].worked_day = 0;
        new_data[key].shift_code = typeof nextProps.shifts[new_data[key].shift-1] !== "undefined" ? nextProps.shifts[new_data[key].shift-1].cod : null;
    });
    return new_data;
}

/**
 * -----------------------
 * VALIDACION 3 DOMINGOS TRABAJADOS SEGUIDOS MAXIMO
 * ALERTA RESTRICTIVA (ROJO)
 * -----------------------
 * @params weeks , nextProps , freeShiftArr
 * return data array
 **/
export function maxThreeContinousWorkedSundays(weeks, nextProps){
    let new_data = [];
    weeks.splice(0,1);
    weeks.map( (w) => {
        let key = w.key*7;
        new_data[key] = {};
        new_data[key].status = 2;
        new_data[key].employee = nextProps.employee.id;
        new_data[key].day_index = key;
        new_data[key].err_msg = "3+ Domingos trabajados";
        new_data[key].shift = nextProps.assignment.employees[nextProps.employee.id].allocations[key].shift;
        new_data[key].worked_day = 1;
        new_data[key].shift_code = typeof nextProps.shifts[new_data[key].shift-1] !== "undefined" ? nextProps.shifts[new_data[key].shift-1].cod : null;
    });
    return new_data;
}

