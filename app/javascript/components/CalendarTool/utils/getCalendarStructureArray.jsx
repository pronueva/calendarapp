/**console
 * Devuelve el objeto de la estructura del Calendario actual
 * @params assignment_code , department , date
 * return Object
 */
export function getCalendarStructureArray( assignment_code , area , date , employees , weeks ){
    let employees_data = [];
    let allocations_data = [];
    let weeks_data = [];
    weeks.map( w => {
        w.days.map(d=>{
            allocations_data[d.day_index] = {
                day_index: d.day_index,
                day: d.day,
                day_number: d.number,
                month: d.month,
                year: d.year,
                shift: null,
                status: 0,
                err_msg: null,
                worked_day: 0
            };
        });
        weeks_data[w.id] = {
            is_sunday_worked: 0,
            is_seventh_day_worked: 0
        };
    });
    employees.map( e => {
        employees_data[e.id] = {
            id: e.id,
            allocations: allocations_data,
            weeks: weeks_data
        }
    });
    const data = {
        code: assignment_code,
        area: area.id,
        date: date,
        employees: employees_data
    };
    return data;
}