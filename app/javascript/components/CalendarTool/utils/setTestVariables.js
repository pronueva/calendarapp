/**
 * Devuelve el contract del empleado
 * @params employee_id
 * return integer
 */
export function setEmployeeContractId(employee_id){
    return (employee_id % 2) === 0 ? 2 : employee_id % 2;
}
/**
 * Devuelve los beneficios del contrato según contract_id
 * @params contract_id
 * return array
 */
export function setContractBenefits(contract_id){
    let benefit = [];
    if(contract_id == 1){
        benefit["id"] = 1;
        benefit["code"] = "DL1";
        benefit["description"] = "primeros dos domingos libres";
    }else{
        benefit["id"] = 2;
        benefit["code"] = "DL3";
        benefit["description"] = "últimos dos domingos libres";
    }
    return benefit;
}