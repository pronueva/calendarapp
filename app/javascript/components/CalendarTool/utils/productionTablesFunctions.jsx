/**
 * Devuelve el objeto de la estructura del Calendario actual para la base de las tablas de productividad
 * @params weeks
 * return Object
 */
export function getCalendarProductionPanelStructureArray( weeks ){
    let weeks_data = [];

    let infoData = [];
    weeks.map( w => {
        w.days.map(d=>{
            infoData[d.day_index] = {
                value:0
            };
        });
        weeks_data[w.id] = {
            is_sunday_worked: 0,
            is_seventh_day_worked: 0
        };
    });
    let infoTable = getInfoTable()
    infoTable.map( e => {
        e.allocations = infoData
    });
    return infoTable;

}
/**
 * Devuelve Array con la información de los tipos de turnos
 * @params
 * return Array
 */
export function getInfoTable(){
    return [
        {id:0 , text:"intermedio"},
        {id:1 , text:"am"},
        {id:2 , text:"pm"},
        {id:3 , text:"free"}
    ];
}
/**
 * Suma o Resta (opt) el valor de el panel de producción de los turnos
 * @params selected_shift , day_index , update_production_panel_data , opt
 * */
export function updateShiftInfoPanel( selected_shift , day_index , update_production_panel_data , opt ){
    let production_data = {};
    switch(opt){
        case "remove":
            if(selected_shift) {
                production_data.day_index = day_index;
                production_data.value = -1;
                production_data.production_key = selected_shift.time;
                update_production_panel_data(production_data);
            }
            break;
        case "add":
            if(selected_shift){
                production_data.day_index = day_index;
                production_data.value = 1 ;
                production_data.production_key = selected_shift.time;
                update_production_panel_data( production_data )
            }
            break;
    }
}
