import React from 'react';
import { connect } from 'react-redux';
import './index.css';
import CalendarContainer from './containers/CalendarContainer';
import InformationContainer  from './components/Panels/InformationContainer'

//actions
import { toggle_loading } from '../../actions/loading';
import { set_assignment } from '../../actions/assignment';

class CalendarTool extends React.Component {
    constructor(){
        super();
        this.state = {
            assignment: {}
        }
    }
    shouldComponentUpdate(nextProps , nextState){
        return nextState.calendar ? nextState.calendar === this.state.calendar : false;
    }
    componentWillUpdate(nextProps , nextState){
        this.props.set_assignment(nextState.calendar);
    }
    componentWillReceiveProps(nextProps){
        this.setState({ assignment: nextProps.assignment , calendar: nextProps.calendar  });
    }
    render() {
        if(!this.props.area){
            return (<div></div>)
        }
        const {currentYear , currentMonth , currentDay } = this.props.date;
        const date = new Date( currentYear , currentMonth , 1 );
        return (
            <div>
                <CalendarContainer
                date={ this.props.date ? date : new Date() }
                area_id={ this.props.area.id ? this.props.area.id : 0 }
                benefits={this.props.benefits}
                shifts={this.props.shifts}
                calendar={this.props.calendar}
                assignment={this.state.assignment}
                />
                <InformationContainer assignment={this.state.assignment}
                                      weeks={this.props.weeks}
                                      employees={this.props.employees}
                                      shifts={this.props.shifts}/>
            </div>
        )
    }
}
//connect
const mapStateToProps = state => ({
    area: state.area,
    assignment: state.assignment,
    weeks: state.calendar.weeks,
    shifts: state.shifts.shifts,
    date: state.date
});
const mapDispatchToProps = (dispatch) => {
    return{
        toggle_loading: () => {
            dispatch(toggle_loading());
        },
        set_assignment: (data) => {
            dispatch(set_assignment(data));
        }
    }
};
export default connect(mapStateToProps,mapDispatchToProps)(CalendarTool);
