import React from 'react';
import { default as Month }  from '../components/Month/Month';

import '../styles/Calendar.css';

export default class CalendarContainer extends React.Component {
    constructor(){
        super();
        this.state = {
            calendar : null
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({ assignment: nextProps.assignment , calendar: nextProps.calendar  });
    }
    render() {
        return (
            <div className="scrollable" id="scrollable">
              <Month
                  weeks={this.props.weeks}
                  shifts={this.props.shifts}
                  benefits={this.props.benefits}
                  calendar={this.state.calendar}
              />
            </div>
        );
    }
}

