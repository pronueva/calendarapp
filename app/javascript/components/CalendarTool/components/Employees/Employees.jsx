import React from 'react';
import { connect } from 'react-redux';
import { default as diff } from 'deep-diff';

import { Grid } from 'semantic-ui-react';
import { default as EmployeeWeek } from './EmployeeWeeks';
import { freeShiftArr } from '../../../../utils/base'; //base del componente

//import { setEmployeeContractId , setContractBenefits } from '../../utils/setTestVariables';

//utils
import { maxThreeContinousWorkedSundays , maxTwoFreeSundays , benefitSundaysValdiation } from '../../utils/sundayValidations';
//actions
import { fetch_area , fetch_employee_benefit } from '../../../../actions/area';
import { update_assignment } from '../../../../actions/assignment';

const options = [
    { key: 1, text: 'S/A', value: "0" }
];

export const styles = {
    headingMonth : {
        textTransform: "capitalize"
    },
    calendarDayGrid: {
        textAlign: "center",
        padding: "0px 2px"
    }
};
class Employees extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            assignment_weeks: [],
            assignment:this.props.assignment,
            employee :this.props.employee
        };
    }
    componentWillMount(){
        const benefit_id = this.props.employee.benefit_id;
        let benefit_arr_key = 0;
        for(let i = 0 ; i < this.props.benefits.length ; i ++){
            if( this.props.benefits[i].id === benefit_id ){
                benefit_arr_key = i;
            }
        }
       this.props.employee.benefit = this.props.benefits[benefit_arr_key];
    }
    componentDidMount(){
        this.setState({
            assignment_weeks: this.props.assignment.employees[this.props.employee.id].weeks,
            assignment: this.props.assignment
        });
    }
    employeesWeeks(){
        return this.props.weeks.map( week  => {
            if(week){
                return(
                    <Grid.Column key={ week.id } className="wide week">
                        <EmployeeWeek key={ week.id } employee={ this.props.employee }
                                      days={ week.days } week={ week } month={ week.month } year={ week.year }
                                      setWorkingSundays={this.setWorkingSundays}
                                      shifts={this.props.shifts}
                                      assignment = {this.state.assignment}
                                      onRef={ref => (this.child = ref)}
                        />
                    </Grid.Column>
                )
            }
        })
    }
    shouldComponentUpdate(nextProps, nextState){
        const state_employee = this.state.assignment.employees[this.state.employee.id];
        const nextState_employee = nextState.assignment.employees[this.state.employee.id];
        return typeof diff( state_employee , nextState_employee ) !== "undefined" ;
    }
    componentWillUpdate(nextProps, nextState){
        const arr_path =  diff( this.props , nextProps )[0].path[4] ;
        const weeks = nextProps.assignment.employees[this.props.employee.id].weeks;
        let worked_sundays = [];
        let free_sundays = [];
        let week_data = {};
        let new_data = [];
        let current_month = nextProps.date.currentMonth;

        const assignments = nextProps.assignment.employees[nextProps.employee.id].allocations;
        let consecutive_worked_days_count = 0; // Cantidad dias trabajados
        let consecutive_worked_sundays_count = 0; // Cantidad domingos trabajados
        let free_sundays_count = 0; // Cantidad domingos libres
        /**
         * SET default SUNDAYS data
         * */
        for (let i = 1; i < weeks.length ; i ++){
            let key = i * 7;
            new_data[key] = {};
            let day_month = nextProps.assignment.employees[nextProps.employee.id].allocations[i*7].month;
            if(current_month === day_month ) // la validacion debe ser con los domingos del mes
            {
                //si domingo anterios === worked_sunday
                if(weeks[i].is_sunday_worked === 1) {
                    consecutive_worked_sundays_count ++;
                    weeks[i].is_sunday_worked === 1 ? worked_sundays.push({key: (i), value: 1})
                        : ""
                }else{
                    consecutive_worked_sundays_count = 0;
                }
            }

            //si domingo es distinto a 7mo dia trabajado
                if(consecutive_worked_sundays_count < 3){
                    if(weeks[i].is_seventh_day_worked !== 1){
                        new_data[key].status = nextProps.assignment.employees[nextProps.employee.id].allocations[key].shift ? 1 : 0;
                        new_data[key].shift = nextProps.assignment.employees[nextProps.employee.id].allocations[key].shift;
                        new_data[key].shift_code = typeof nextProps.options[new_data[key].shift] !== "undefined" ? nextProps.options[new_data[key].shift].cod : null;
                        new_data[key].err_msg = Object.values(freeShiftArr).indexOf(new_data[key].shift_code) > -1 ? "Domingo libre" : new_data[key].shift_code ? "Domingo Trabajado" : null;
                        new_data[key].worked_day = Object.values(freeShiftArr).indexOf(new_data[key].shift_code) > -1 ? 0 : new_data[key].shift_code ? 1 : 0;
                        new_data[key].day_index = key;
                        new_data[key].employee = nextProps.employee.id;
                        benefitSundaysValdiation(nextProps.employee, nextProps.weeks[i], new_data[key], freeShiftArr);

                        let deepdiff = diff(new_data[key], nextProps.assignment.employees[nextProps.employee.id].allocations[new_data[key].day_index]);
                        let differences = deepdiff.filter(function (dd) {
                            return dd.kind === "E";
                        });
                        differences.map((ddiff) => {
                            new_data[key][ddiff.path[0]] = ddiff.lhs;
                            new_data[key].updatable = 1;
                        });
                    }else{
                        new_data[key].status = nextProps.assignment.employees[nextProps.employee.id].allocations[key].shift ? 2 : 0;
                        new_data[key].shift = nextProps.assignment.employees[nextProps.employee.id].allocations[key].shift;
                        new_data[key].shift_code = typeof nextProps.options[new_data[key].shift] !== "undefined" ? nextProps.options[new_data[key].shift].cod : null;
                        new_data[key].err_msg = Object.values(freeShiftArr).indexOf(new_data[key].shift_code) > -1 ? "Domingo libre" : new_data[key].shift_code ? "7+ días trabajados" : null;
                        new_data[key].worked_day = Object.values(freeShiftArr).indexOf(new_data[key].shift_code) > -1 ? 0 : new_data[key].shift_code ? 1 : 0;
                        new_data[key].day_index = key;
                        new_data[key].employee = nextProps.employee.id;

                        let deepdiff = diff(new_data[key], nextProps.assignment.employees[nextProps.employee.id].allocations[new_data[key].day_index]);
                        let differences = deepdiff.filter(function (dd) {
                            return dd.kind === "E";
                        });
                        differences.map((ddiff) => {
                            new_data[key][ddiff.path[0]] = ddiff.lhs;
                            new_data[key].updatable = 1;
                        });
                    }
                }
        }

        consecutive_worked_sundays_count = 0;

        for(let i = 1; i < assignments.length ; i++){
            let current_day = assignments[i];

            if(i%7 !== 0) {
                new_data[current_day.day_index] = {};
            }
            if(current_day.worked_day > 0){
                consecutive_worked_days_count ++;
            }
            if(current_day.worked_day === 0){
                consecutive_worked_days_count = 0;
            }

            /**
             * -----------------------
             * VALIDACIONES DOMINGOS
             * -----------------------
             **/
            // SI DIA CAMBIADO === DOMINGO
            if(current_day.day === 0 ){

                // contar domingos trabajados seguidos
                /**
                 * -----------------------
                 * VALIDACION 3 DOMINGOS TRABAJADOS SEGUIDOS MAXIMO
                 * ALERTA RESTRICTIVA (ROJO)
                 * -----------------------
                 **/
                let day_month = nextProps.assignment.employees[nextProps.employee.id].allocations[i].month;
                if(current_month === day_month ) // la validacion debe ser con los domingos del mes
                {
                    //si domingo anterios === worked_sunday
                    if(weeks[i/7].is_sunday_worked === 1) {
                        consecutive_worked_sundays_count ++;
                        worked_sundays[i / 7] = {key: (i / 7), value: 1};
                    }else{
                        consecutive_worked_sundays_count = 0;
                        if(current_day.shift){
                            free_sundays_count ++;
                            free_sundays[i / 7] = {key: (i / 7), value: 0};
                        }
                    }
                }
                if(consecutive_worked_sundays_count > 3){
                    let data = maxThreeContinousWorkedSundays(worked_sundays , nextProps);
                    data.map((d) => {
                        let deepdiff = diff( d , nextProps.assignment.employees[nextProps.employee.id].allocations[d.day_index] )
                        let differences = deepdiff.filter(function (dd) {
                            return dd.kind === "E";
                        });
                        new_data[d.day_index].day_index = d.day_index;
                        new_data[d.day_index].employee = d.employee;
                        new_data[d.day_index].err_msg = d.err_msg;
                        new_data[d.day_index].shift = d.shift;
                        new_data[d.day_index].status = d.status;
                        new_data[d.day_index].worked_day = d.worked_day;
                        new_data[d.day_index].updatable = 0;
                        differences.map( (ddiff) => {
                            new_data[d.day_index][ddiff.path[0]] = ddiff.lhs;
                            new_data[d.day_index].updatable = 1;
                        })
                    });
                }
                /**
                 * -----------------------
                 * VALIDACION 2 DOMINGOS LIBRES SEGUIDOS MAXIMO
                 * ALERTA DE PRODUCTIVIDAD (AMARILLO)
                 * -----------------------
                 **/
                if(free_sundays_count > 2) {
                    let data = maxTwoFreeSundays(free_sundays , nextProps);
                    data.map((d) => {
                        let deepdiff = diff(d, nextProps.assignment.employees[nextProps.employee.id].allocations[d.day_index])
                        let differences = deepdiff.filter(function (dd) {
                            return dd.kind === "E";
                        });
                        if (current_day.status !== 2) {
                            new_data[d.day_index].day_index = d.day_index;
                            new_data[d.day_index].employee = d.employee;
                            new_data[d.day_index].err_msg = d.err_msg;
                            new_data[d.day_index].shift = d.shift;
                            new_data[d.day_index].status = d.status;
                            new_data[d.day_index].worked_day = d.worked_day;
                            new_data[d.day_index].updatable = 0;
                            differences.map((ddiff) => {
                                new_data[d.day_index][ddiff.path[0]] = ddiff.lhs;
                                new_data[d.day_index].updatable = 1;
                            })
                        }
                    });
                }
                /**
                 * -----------------------
                 * VALIDACIONES OFF dia = domingo
                 * ALERTA RESTRICTIVA (ROJO)
                 * -----------------------
                 **/
                if(consecutive_worked_days_count === 7){
                    //si no hay otra alerta (rojo) en el turno status !== 2
                    if(current_day.status !== 2){
                        new_data[current_day.day_index].status = 2;
                        new_data[current_day.day_index].day_index = current_day.day_index;
                        new_data[current_day.day_index].err_msg = "7+ días trabajados";
                        new_data[current_day.day_index].shift = current_day.shift;
                        new_data[current_day.day_index].employee = nextProps.employee.id;
                        new_data[current_day.day_index].worked_day = 1;
                        new_data[current_day.day_index].updatable = 1;
                        //week data
                        week_data.week = i/7;
                        week_data.worked_day = 1;
                        week_data.employee = nextProps.employee.id;
                        week_data.is_seventh_day_worked =  1;
                        this.child.setWorkingSundays(week_data);
                    }
                }else{

                    //week data
                    new_data[current_day.day_index].shift_code = typeof nextProps.options[current_day.shift] !== "undefined" ? nextProps.options[current_day.shift].cod : null;

                    week_data.week = i/7;
                    week_data.worked_day =  new_data[i].shift_code ? Object.values(freeShiftArr).indexOf(new_data[i].shift_code) > -1 ? 0 : 1 : 0;

                    week_data.employee = nextProps.employee.id;
                    week_data.is_seventh_day_worked = 0;
                    this.child.setWorkingSundays(week_data);
                }
            }else{
                /**
                 * -----------------------
                 * VALIDACIONES OFF dia != domingo
                 * ALERTA RESTRICTIVA (ROJO)
                 * -----------------------
                 **/
                if(consecutive_worked_days_count === 7){
                    new_data[current_day.day_index].status = 2;
                    new_data[current_day.day_index].day_index = current_day.day_index;
                    new_data[current_day.day_index].err_msg = "7+ días trabajados";
                    new_data[current_day.day_index].shift = current_day.shift;
                    new_data[current_day.day_index].employee = nextProps.employee.id;
                    new_data[current_day.day_index].worked_day = 1;
                    new_data[current_day.day_index].updatable = 1;
                }else{
                    let is_changed_data = 0;
                    new_data[current_day.day_index].status =  Object.values(freeShiftArr).indexOf(new_data[i].shift_code) === -1 ? 1 : 0;
                    if( current_day.status !== 2 ){
                        is_changed_data = this.props.assignment.employees[nextProps.employee.id].allocations[current_day.day_index].shift !== current_day.shift;
                    }else{
                        is_changed_data = new_data[current_day.day_index].status !== current_day.status;
                    }
                    new_data[current_day.day_index].day_index = current_day.day_index;
                    new_data[current_day.day_index].err_msg = "";
                    new_data[current_day.day_index].shift = current_day.shift;
                    new_data[current_day.day_index].employee = nextProps.employee.id;
                    new_data[current_day.day_index].worked_day = current_day.worked_day;
                    new_data[current_day.day_index].updatable =  is_changed_data ? 1 : 0;
                }
            }
            consecutive_worked_days_count = consecutive_worked_days_count === 7 ? 0 : consecutive_worked_days_count;
        }

        new_data.map( (nd) => {
            if( nd.updatable === 1 ) {
                this.props.update_assignment(nd)
            }
        });
    }
    componentWillReceiveProps(nextProps){
        /**
         * -----------------------
         *  VALIDACIONES DOMINGOS
         * -----------------------
         **/
        this.setState({
            assignment_weeks:nextProps.assignment.employees[this.props.employee.id].weeks,
            assignment: nextProps.assignment
        })
    }
    render(){
        return(
            <Grid.Row className="week-row">
                <div className="wide employee column week">{this.props.employee.first_name + " " + this.props.employee.last_name}</div>
                { this.props.used_weeks > 0 ? this.employeesWeeks() : "" }
            </Grid.Row>
        )
    }
}
//connect
const mapStateToProps = state => ({
    employees: state.area.employees,
    assignment: state.assignment,
    date: state.date,
    options: state.shifts.options,
    benefits: state.benefits,
    weeks: state.calendar.weeks,
    used_weeks: state.calendar.used_weeks,
    area: state.area
});
const mapDisptachToProps = (dispatch) => {
    return {
        fetch_area: (area_id) =>{
            dispatch(fetch_area(area_id));
        },
        fetch_employee_benefit: (benefit_id) =>{
            dispatch(fetch_employee_benefit(benefit_id));
        },
        update_assignment: (data) => {
            dispatch(update_assignment(data));
        }
    }
};
export default connect(mapStateToProps,mapDisptachToProps)(Employees);