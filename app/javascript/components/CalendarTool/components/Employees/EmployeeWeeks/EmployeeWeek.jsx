import React from 'react';
import { connect } from 'react-redux';
import { styles  , freeShiftArr } from '../../../../../utils/base'; //base del componente
import { getSelectedShift } from '../../../utils/shiftUtils';

import { default as EmployeeDay } from './EmployeeDays/EmployeeDay';
import { Grid } from 'semantic-ui-react';

//actions
import { update_worked_sundays , update_sum_hours } from '../../../../../actions/assignment';

class EmployeeWeek extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            employee: this.props.employee.id,
            week_id: this.props.week.id,
            weekly_sum: 0,
            is_completed: false,
            assignment: this.props.assignment,
        };
        this.setWorkingSundays = this.setWorkingSundays.bind(this);
    }
    componentWillMount(){
        this.setState({
            assignment: this.props.assignment,
        });
    }
    componentWillUnmount(){
        this.props.onRef(null);
    }
    componentDidMount(){
        this.setAssignedDaysOnWeek();
        this.props.onRef(this);
    }
    setAssignedDaysOnWeek(){
        const week_assignments = this.state.assignment.employees[this.state.employee];
        let week_allocations = week_assignments.allocations.filter( (w) => {
            return Math.ceil(w.day_index / 7)  === this.state.week_id
        });
        let assigned_days = week_allocations.filter((w)=>{
            return w.shift
        });
        if(assigned_days.length === 7 ) {
            this.setState({
                is_completed: true
            })
        }else{
            this.setState({
                is_completed: false
            });
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            assignment: nextProps.assignment,
            weekly_sum: nextProps.assignment.employees[this.state.employee].weeks[this.state.week_id].weekly_sum
        });
    }
    shouldComponentUpdate(nextProps , nextState){
        return JSON.stringify(nextState.assignment.employees[this.state.employee].allocations) !== JSON.stringify(this.state.assignment.employees[this.state.employee].allocations) || JSON.stringify(nextState.weekly_sum) !== JSON.stringify(this.state.weekly_sum);
    }
    setWorkingSundays(data){
        this.props.update_worked_sundays(data);
    }
    setWeeklySum(data , selected_shift){
        let new_data = this.state;
        console.log('*************')
        console.log(new_data)
        if(typeof selected_shift !== "undefined"){
            new_data.weekly_sum -= this._sumHours(selected_shift);
        }
        console.log(new_data.weekly_sum)
        this.setAssignedDaysOnWeek();
        let shift = getSelectedShift(this.props.shifts, data.shift);

        new_data.weekly_sum += this._sumHours(shift);

        this.props.update_sum_hours(new_data)
        console.log(new_data.weekly_sum);
        console.log('*************')
        /*this.setState({
            weekly_sum: new_data.weekly_sum
        });*/
    }
    _sumHours(shift){
        let shift_start = shift.shift_start.split('T')[1].split(".")[0];
        shift_start =  Object.values(freeShiftArr).indexOf(shift.cod) < 0 ? ( shift_start.split(":")[0] === "00" ? 0
                : parseInt(shift_start.split(":")[0]) ) + (parseInt(shift_start.split(":")[1]) / 60) : 0;
        let shift_end = shift.shift_end.split('T')[1].split(".")[0];
        shift_end =  Object.values(freeShiftArr).indexOf(shift.cod) < 0 ? ( shift_end.split(":")[0] === "00" ? 24
                : parseInt(shift_end.split(":")[0]) ) + (parseInt(shift_end.split(":")[1]) / 60) : 0;
        if(shift_end < shift_start){
            shift_end = 24 + shift_end;
        }

        let diff = shift_end - shift_start ;
        diff = diff < 0 ? diff * -1 : diff;
        return diff;
    }
    getWeekDays(){
        const employee = this.state.employee;
        return this.props.days.map( day => {
            if(day){
                let assignment = this.state.assignment.employees[employee].allocations[day.day_index];
                return(
                    <EmployeeDay key={day.number}
                                 employee={ this.props.employee }
                                 day={day} week={this.props.week} month={day.month}
                                 setWorkingSundays={(data)=>this.setWorkingSundays(data)}
                                 setWeeklySum={(data,selected_shift)=>this.setWeeklySum(data,selected_shift)}
                                 assignment={this.state.assignment}
                                 shifts={this.props.shifts}
                                 shift={assignment.shift}
                    />
                )
            }
        })
    }

    render(){
        let statusClass = this.state.weekly_sum !== 51 && this.state.is_completed ? "err pulse" : "default";
        return(
            <Grid columns="equal">
                { this.getWeekDays() }
                <Grid.Column style={styles.calendarDayGrid}>
                    <div className={"weekly_sum " + statusClass}>{Math.round(this.state.weekly_sum * 10)/10}</div>
                </Grid.Column>
            </Grid>
        )
    }
}
//connect
const mapStateToProps = state => ({
    assignment: state.assignment,
});
const mapDispatchToProps = (dispatch) => {
    return{
        update_worked_sundays: (data) =>{
            dispatch(update_worked_sundays(data));
        },
        update_sum_hours: (data) => {
            dispatch(update_sum_hours(data));
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(EmployeeWeek)