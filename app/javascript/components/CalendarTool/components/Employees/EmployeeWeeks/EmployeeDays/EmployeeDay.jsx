import React from 'react';
import { connect } from 'react-redux';
import { Base ,  styles , freeShiftArr } from '../../../../../../utils/base'; //base del componente

import { Grid , Dropdown } from 'semantic-ui-react';

//utils
import { benefitSundaysValdiation } from '../../../../utils/sundayValidations';
import { getSelectedShift } from '../../../../utils/shiftUtils';
import { updateShiftInfoPanel } from '../../../../utils/productionTablesFunctions';

//actions
import { update_assignment , update_production_panel_data } from '../../../../../../actions/assignment';

class EmployeeDay extends Base{
    constructor(props){
        super(props);
        this.state = {
            assignment: this.props.assignment.employees[this.props.employee.id],
            options: [],
            loader: false,
            day_index: this.props.day.day_index,
            day: this.props.day.day,
            week: this.props.week.id,
            number: this.props.day.number,
            employee: this.props.employee.id,
            month: this.props.month,
            status: null,
            shift:null,
            input_class: "",
            worked_day: 0,
            err_msg: null,
            is_sunday: this.props.day.day <= 0 //true == domingo || false == !domingo
        };
        this._handleChange = this._handleChange.bind(this);
    }
    componentDidMount(){
        this.updateShiftInfoPanel("remove",this.state.shift);
        let new_data = {};
        let options = this.state.options;
        new_data.status = 0;
        this.props.options.map(option => {
            //IDENTIFICA SI ES DOMINGO
            if(this.state.day === 0){
                //SI ESTA DENTRO DEL CONTRATO LO MARCA COMO DOMINGO LIBRE D/L
                if(this.props.employee.benefit.cod === "DL1"){
                    if(this.props.week.id === 1 || this.props.week.id === 2){
                        this.setState({
                            shift: 1,
                            input_class: "free sunday"
                        });
                        new_data.shift = 1;
                        new_data.status = 1;
                        new_data.err_msg = "Domingo libre por contrato";
                        new_data.day_index = this.state.day_index;
                        new_data.employee = this.props.employee.id;
                        new_data.worked_day = 0;
                        //actualizar store
                        this.props.update_assignment( new_data );
                    }
                }
                if(this.props.employee.benefit.cod === "DL2"){
                    if(this.props.week.id === 2 || this.props.week.id === 3){
                        this.setState({
                            shift: 1,
                            input_class: "free sunday"
                        });
                        new_data.shift = 1;
                        new_data.status = 1;
                        new_data.err_msg = "Domingo libre por contrato";
                        new_data.day_index = this.state.day_index;
                        new_data.employee = this.props.employee.id;
                        new_data.worked_day = 0;
                        //actualizar store
                        this.props.update_assignment( new_data );
                    }
                }
                if(this.props.employee.benefit.cod === "DL3"){
                    if(this.props.week.id === 3 || this.props.week.id === 4){
                        this.setState({
                            shift: 1,
                            input_class: "free sunday"
                        });
                        new_data.shift = 1;
                        new_data.status = 1;
                        new_data.err_msg = "Domingo libre por contrato";
                        new_data.day_index = this.state.day_index;
                        new_data.employee = this.props.employee.id;
                        new_data.worked_day = 0;
                        //actualizar store
                        this.props.update_assignment( new_data );
                    }
                }
                if(this.props.employee.benefit.cod === "DL4"){
                    if(this.props.week.id === 3 || this.props.week.id === 4){
                        this.setState({
                            shift: 1,
                            input_class: "free sunday"
                        });
                        new_data.shift = 1;
                        new_data.status = 1;
                        new_data.err_msg = "Domingo libre por contrato";
                        new_data.day_index = this.state.day_index;
                        new_data.employee = this.props.employee.id;
                        new_data.worked_day = 0;
                        //actualizar store
                        this.props.update_assignment( new_data );
                    }
                }
                //SI TURNOS ES "D/L" NO LO MUESTRA EN DIAS > 0
                if(option.text !== "OFF" ) {
                    options.push({
                        key: option.key + "" + this.props.week.id + "" + this.props.day.day_index + "" + this.props.employee.id,
                        text: option.text,
                        type: option.type,
                        value: option.key,
                    })
                }
            }else{
                //SI TURNOS ES "D/L" NO LO MUESTRA EN DIAS > 0
                if(option.text !== "D/L" ){
                    options.push({
                        key: option.key+""+this.props.week.id+""+this.props.day.day_index+""+this.props.employee.id,
                        text: option.text,
                        type: option.type,
                        value: option.key,
                    })
                }
            }
        });
        this.setState({
            options: options
        });
        this.updateShiftInfoPanel("add" , new_data.shift)
    }
    updateShiftInfoPanel(opt , data){
        let selected_shift = getSelectedShift(this.props.shifts,data);
        updateShiftInfoPanel( selected_shift , this.state.day_index , this.props.update_production_panel_data , opt );
    }
    _handleChange(e,data){
        this.updateShiftInfoPanel("remove",this.state.shift);
        this.setState({shift:data.value});

        const { week } = this.props;
        let new_data = {};
        new_data.shift = data.value;
        new_data.shift_code = this.props.options[new_data.shift].cod;
        new_data.status = 1;
        new_data.month = this.state.month;
        new_data.year = this.state.year;
        new_data.day_index = this.props.day.day_index;
        new_data.err_msg = null;
        new_data.week = week.id;
        new_data.employee = this.props.employee.id;
        new_data.worked_day = this.state.worked_day ? this.state.worked_day : 0;

        if(Object.values(freeShiftArr).indexOf(new_data.shift_code) < 0){
            new_data.worked_day = 1
        }else{
            new_data.worked_day = 0
        }
        /**
         * -----------------------
         *  VALIDACIONES DOMINGOS
         *  this.state.day = 0
         * -----------------------
        **/
        if(this.state.is_sunday){
            // VALIDAR 3 DOMINGOS TRABAJADOS
            // domingos trabajados
            if(Object.values(freeShiftArr).indexOf(new_data.shift_code) < 0){
                new_data.status = 1; //default
                new_data.err_msg = "Domingo trabajado";
                new_data.worked_day = 1;
                this.props.setWorkingSundays(new_data);
            }
            //domingos libre
            if(Object.values(freeShiftArr).indexOf(new_data.shift_code) > -1){
                new_data.status = 1; //default
                new_data.err_msg = null;
                new_data.worked_day = 0;
                this.props.setWorkingSundays(new_data);
            }
            //validar domingos por contrato
            benefitSundaysValdiation( this.props.employee , this.props.week , new_data , freeShiftArr );
        }
        this.setState({
            new_data
        });

        this.props.update_assignment( new_data );
        this.props.setWeeklySum( new_data , getSelectedShift(this.props.shifts,this.state.shift) );

        this.updateShiftInfoPanel("add" , data.value)
    }/*
    shouldComponentUpdate(nextProps, nextState){
        const { assignment , employee , day } = nextProps;
        const shift = nextProps.shift;
        // return a boolean value
        console.log(JSON.stringify(this.state.assignment.allocations[day.day_index]) !== JSON.stringify(nextProps.assignment.employees[employee.id].allocations[day.day_index]) || this.state.shift !== nextState.shift)
        return JSON.stringify(this.state.assignment.allocations[day.day_index]) !== JSON.stringify(nextProps.assignment.employees[employee.id].allocations[day.day_index]) || this.state.shift !== nextState.shift;
    }
    componentWillUpdate(nextProps, nextState){
        console.log(nextState)
        let new_data = {};
        new_data.shift = nextState.shift;
        new_data.shift_code = nextProps.options[nextState.shift].cod;
        new_data.status = 1;
        new_data.month = nextState.month;
        new_data.year = nextState.year;
        new_data.day_index = nextState.day_index;
        new_data.err_msg = null;
        new_data.week = nextProps.week.id;
        new_data.employee = nextProps.employee.id;
        new_data.worked_day = nextState.worked_day ? nextState.worked_day : 0;

        nextProps.setWeeklySum( new_data , getSelectedShift(nextProps.shifts,nextState.shift));
    }*/
    componentWillReceiveProps(nextProps){
        const { assignment , employee , shift } = nextProps;
        /**
         * -----------------------
         *  VALIDACIONES DOMINGOS
         * -----------------------
         **/
        this.setState({
            assignment: assignment.employees[employee.id],
            shift: shift
        })
    }
    render(){
        const { assignment , employee , day } = this.props;
        let statusClass = "";
        if(Object.keys(assignment).length > 0 ){
            const assignment_data = assignment.employees[employee.id].allocations[day.day_index];
            statusClass = assignment_data.status === 1 ? "success" : assignment_data.status === 2 ? "err pulse" : assignment_data.status === 3 ? "warning pulse" : assignment_data.status === 4 ? "info pulse" : "";
        }
        return(
            <Grid.Column style={styles.calendarDayGrid}>
                <Dropdown search selection
                          placeholder="S/A"
                          className={"month " + this.state.input_class + " " + statusClass}
                          style={styles.dayCombobox}
                          options={this.state.options}
                          icon={null}
                          onChange={ (e,data) => this._handleChange(e,data)}
                          value={this.state.shift}
                />
            </Grid.Column>
        )
    }
}
//connect
const mapStateToProps = state =>({
    options: state.shifts.options,
    shifts: state.shifts.shifts,
    area_id: state.area_id
});
const mapDispatchToProps = (dispatch) => {
    return{
        update_assignment: (data) =>{
            dispatch(update_assignment(data));
        },
        update_production_panel_data: (data) =>{
            dispatch(update_production_panel_data(data));
        },
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(EmployeeDay)