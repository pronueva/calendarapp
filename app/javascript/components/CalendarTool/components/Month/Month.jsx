import React from 'react';
import { connect } from 'react-redux';
import { styles , calendarDate } from '../../../../utils/base'; //base del componente

import { Week } from './Weeks'
import { LoadingDataComponent } from '../../../LoaderComponent/LoadingDataComponent'
import { default as Employees } from '../Employees'
import { Grid } from 'semantic-ui-react';

//utils
import { getMonthUsedWeeks , getMonthWeeks } from '../../utils/getCalendarMonthWeeks';
import { getCalendarStructureArray } from '../../utils/getCalendarStructureArray';
import { getCalendarProductionPanelStructureArray } from '../../utils/productionTablesFunctions';
//actions
import { fetch_employee_benefit } from '../../../../actions/area';
import { set_assignments_structure , set_production_panel_structure} from '../../../../actions/assignment';
import { set_current_month_used_weeks , set_current_month_name , set_current_month_weeks } from '../../../../actions/calendar';
import { set_shifts_options } from '../../../../actions/shifts';

class Month extends React.Component{
    constructor(){
        super();
        this.state = {
            assignment: {}
        }
    }
    componentDidMount(){
        let yearNumber = this.props.date.currentYear; // año
        let monthNumber = this.props.date.currentMonth; // numero del mes actual

        let firstDayOfMonth = new Date(yearNumber , monthNumber, 1).getDay(); // primer dia del mes
        let days = new Date(yearNumber , monthNumber+1 , 0 ).getDate(); // numero de dias del mes
        let weeks = getMonthUsedWeeks(days , firstDayOfMonth);

        this.props.set_current_month_weeks(getMonthWeeks(  yearNumber ,  monthNumber , weeks ));
        this.props.set_current_month_used_weeks(weeks);

        const options = [];
        this.props.shifts.map(shift => {
            options[shift.id] = {key: shift.id , text:shift.cod  , cod:shift.cod , value:shift.id, type:"shift"};
        });
        this.props.set_shifts_options(options);

        this.props.set_current_month_name(calendarDate.Months[monthNumber]);

        const { area , date , employees } = this.props;
        const assignment_code = area.name+'_'+area.id+'_'+date.currentYear+'_'+(date.currentMonth + 1);
        this.props.set_assignments_structure( getCalendarStructureArray(assignment_code , area , date , employees , getMonthWeeks(  yearNumber ,  monthNumber , weeks )) )
        this.props.set_production_panel_structure( getCalendarProductionPanelStructureArray( getMonthWeeks(  yearNumber ,  monthNumber , weeks )) )
    }
    weeks(){
        return this.props.weeks.map( week  => {
            if(week){
                return(
                    <Grid.Column key={ week.id } className="wide week">
                        <Week days={ week.days } week={ week } month={ week.month } year={ week.year } />
                    </Grid.Column>
                )
            }
        })
    }
    componentWillReceiveProps(nextProps){
        this.setState({ assignment: nextProps.assignment , calendar: nextProps.calendar });
    }
    render(){
        return(
            <div  id="calendarContainer">
                <Grid centered divided  columns={ this.props.used_weeks } className="month" style={{backgroundColor: "#fff"}}>
                    <Grid.Row centered>
                        <h1 className={"header"} style={styles.headingMonth} >{ calendarDate.Months[this.props.date.currentMonth] + ' ' + this.props.date.currentYear  }</h1>
                    </Grid.Row>
                    <Grid.Row className="week-row">
                        <div className="wide employee column week">Nomina</div>
                        { this.props.used_weeks > 0 ? this.weeks() : "" }
                    </Grid.Row>
                    { Object.keys(this.state.assignment).length > 0 ?
                        this.props.employees.map( employee => {
                            return(
                                <Employees key={employee.id} employee={employee} shifts={this.props.shifts} calendar={this.state.calendar? this.state.calendar : null} />
                            )
                        } )
                        :
                        <LoadingDataComponent type={1} />
                    }
                </Grid>
            </div>
        )
    }
}

//connect
const mapStateToProps = state => ({
    employees: state.area.employees,
    assignment: state.assignment,
    date: state.date,
    month_name: state.calendar.month_name,
    weeks: state.calendar.weeks,
    used_weeks: state.calendar.used_weeks,
    area: state.area
});
const mapDispatchToProps = (dispatch) => {
    return {
        set_assignments_structure: (data) =>{
            dispatch(set_assignments_structure(data));
        },
        set_production_panel_structure: (data) =>{
            dispatch(set_production_panel_structure(data));
        },
        set_current_month_weeks: (weeks) =>{
            dispatch(set_current_month_weeks(weeks));
        },
        set_current_month_used_weeks: (used_weeks) =>{
            dispatch(set_current_month_used_weeks(used_weeks));
        },
        set_current_month_name: (month_name) =>{
            dispatch(set_current_month_name(month_name));
        },
        fetch_employee_benefit: (benefit_id) =>{
            dispatch(fetch_employee_benefit(benefit_id));
        },
        set_shifts_options: (option) =>{
            dispatch(set_shifts_options(option));
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Month);