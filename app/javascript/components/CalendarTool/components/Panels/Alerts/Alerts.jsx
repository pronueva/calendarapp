import React from 'react';
import { connect } from 'react-redux';

import { Divider } from 'semantic-ui-react';
import { AlertEmployees } from './AlertEmployees/AlertEmployees';
import { LoadingDataComponent } from '../../../../LoaderComponent/LoadingDataComponent';

class Alerts extends React.Component{
    constructor(){
        super();
        this.state = {
            alerts: [],
            employees: [],
            assignments: []
        }
    }
    componentWillMount(){
        this.setState({ assignments: this.props.assignment });
        this.setState({ employees: this.props.employees });
    }
    shouldComponentUpdate(nextProps,nextState){
        return  Object.keys(nextState.assignments).length > 0;
    }
    componentWillReceiveProps(nextProps){
        this.setState({ assignments: nextProps.assignment  });
    }
    render(){
        const ulStyle={
            display: "flex"
        };
        return(
            <div>
                <h1>Alertas</h1>
                <Divider />
                    <div style={ulStyle}>
                    {
                        Object.keys(this.state.assignments).length > 0 ?
                            this.state.employees.map(e => {
                                return (
                                   <AlertEmployees key={e.id} employee={e} weeks={this.props.weeks} assignments={this.state.assignments}
                                                   shifts={this.props.shifts} options={this.props.options} />
                                )
                            })
                        :
                            <LoadingDataComponent type={1}/>
                    }
                </div>
            </div>
        )
    }
}

//connect
const mapStateToProps = state => ({
    assignment: state.assignment,
    options: state.shifts.options,
    employees: state.area.employees
});

export default connect(mapStateToProps,null)(Alerts);