import React from 'react';

import { List } from 'semantic-ui-react';

import { AlertEmployeesDay } from './AlertEmployeesDay/AlertEmployeesDay';

export class AlertEmployees extends React.Component{
    constructor(){
        super();
        this.state = {
            assignments: []
        }
    }
    componentWillMount(){
        this.setState({
            assignments: this.props.assignments
        })
    }
    shouldComponentUpdate(nextProps,nextState){
        return  Object.keys(nextState.assignments).length > 0
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            assignments: nextProps.assignments
        })
    }
    render(){
        const listStyle={
            width: "25%",
            flex: "25%",
            minWidth: "25%",
            margin: "0",
            padding: "0 5px"
        };
        const { employee } = this.props;
        return(
            <List divided relaxed style={listStyle}>
                <h1> {employee.first_name}  </h1>
                <span>Informe de cambios</span>
                {
                        this.state.assignments.employees[employee.id].allocations.map(a => {
                            if(a){
                                return (
                                    <AlertEmployeesDay key={a.day_index} assignments={this.state.assignments} assignment={a}
                                                       employee={employee}
                                                       weeks={this.props.weeks}
                                                       shifts={this.props.shifts}
                                                       options={this.props.options} />
                                )
                            }
                        })
                }
            </List>
        )
    }
}