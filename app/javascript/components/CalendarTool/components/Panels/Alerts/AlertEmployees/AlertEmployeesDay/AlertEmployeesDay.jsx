import React from 'react';
import update from 'react-addons-update';
import { calendarDate  } from '../../../../../../../utils/base'; //base del componente

import { List , Label , Icon } from 'semantic-ui-react';

export class AlertEmployeesDay extends React.Component{
    constructor(){
        super();
        this.state = {
            assignments: [],
        }
    }
    componentWillMount(){
        this.setState({
            assignments: this.props.assignments,
            employee: this.props.employee
        })
    }
    shouldComponentUpdate(nextProps,nextState){
        return  Object.keys(nextState.assignments).length > 0
    }
    componentWillReceiveProps(nextProps){
        const { employee , assignment } = nextProps;
        update(this.state.assignments,{
            employees:{
                [employee.id]: {
                    allocations:{
                        [assignment.day_index]:{
                            shift: {$set: assignment.shift}
                        }
                    }
                }
            }
        });
        this.setState({
            assignments: nextProps.assignments
        })
    }
    //devuelve el nombre del mes
    monthName( day_index ){
       let month =  this.searchByDay(day_index);
       return calendarDate.Months[ month ]
    }
    //devuelve el nombre del mes en base al día
    searchByDay( day_index ){
        const { weeks } = this.props;
        let month;
        for (let i=1; i < weeks.length; i++) {
            for (let x=1; x < weeks[i].days.length; x++) {
                if (weeks[i].days[x].day_index === day_index) {
                    month = weeks[i].days[x].month;
                    break;
                }
            }
        }
        return month;
    }
    //devuelve el nombre del dia
    dayName( day ){
        return calendarDate.WeekDays[ day ]
    }
    //devuelve el codigo del turno
    shiftCod( shift ){
        return this.props.options[shift].cod;
    }
    render(){
        const { assignment } = this.props;
        const styles={
            shift:{
                fontWeight:"600"
            }
        };
        const statusClass = assignment.status === 2 ? "err pulse" : assignment.status === 3 ? "warning pulse" : assignment.status === 1 ? "success" : assignment.status === 4 ? "info pulse" : "default";
        return(
            <List.Item className={"alert-panel "+statusClass}>
                <List.Content>
                    <List.Header as='a'>
                        <Label>
                            <Icon name='calendar outline' />
                            Mes: {this.monthName( assignment.day_index )}</Label>
                        <Label>Día: {this.dayName(assignment.day) }  {assignment.day_number}</Label>
                    </List.Header>
                    <div className={"divider"}/>
                    <List.Description as='a'>
                        <List.Icon name='wait' size='small' verticalAlign='middle' />
                        Turno:  <span style={styles.shift}>{ assignment.shift ? this.shiftCod(assignment.shift) : "-" }</span>
                        <br/>
                        <List.Icon name='warning' size='small' verticalAlign='middle' />
                        Err: <span className={"shift_msg"} style={styles.shift}>{ assignment.err_msg ? assignment.err_msg : "-" }</span>
                    </List.Description>
                </List.Content>
            </List.Item>
        )
    }
}