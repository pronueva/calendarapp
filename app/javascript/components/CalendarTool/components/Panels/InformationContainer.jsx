import React from 'react';
import { Icon } from 'semantic-ui-react'
import { default as Alerts } from './Alerts'
import { default as ProductionPanel } from './ProductionPanel'

export default class InformationContainer extends React.Component {
    constructor(props){
        super(props);
        this.state={
            selectedTab: "1"
        };
        this._handleClick = this._handleClick.bind(this);
    }
    componentWillMount(){
    }
    _handleClick(e){
        let id = e.target.getAttribute('data-id');
        this.setState({
            selectedTab: id
        });
    }
    tabPanels(){
        let x = [];
        for(let i = 1 ; i <= document.getElementsByClassName('item panels').length ; i ++){

            x[i] =
                i === 1 ?
                <div className={this.state.selectedTab === i.toString() ? "ui bottom attached active tab segment":"ui bottom attached tab segment"} data-tab={"tab-"+i} key={i}>
                    <Alerts
                        employees={this.props.employees}
                        weeks={this.props.weeks}
                        shifts={this.props.shifts}
                    />
                </div>
                    :
                <div className={this.state.selectedTab === i.toString() ? "ui bottom attached active tab segment":"ui bottom attached tab segment"} data-tab={"tab-"+i} key={i}>
                   <ProductionPanel
                       employees={this.props.employees}
                       weeks={this.props.weeks}
                       shifts={this.props.shifts}
                   />
                </div>

        }
        return x;
    }
    render(){
        return(
            <div>
                <div className="ui tabular menu">
                    <div className={this.state.selectedTab === "1" ? "item attached panels active tab segment":"item attached panels tab segment"}
                         data-tab="tab-1"
                         data-id="1"
                         onClick={ (e) => this._handleClick(e) }
                    >
                        <Icon name="warning sign" color="orange" corner/>
                        Alertas
                    </div>
                    <div className={this.state.selectedTab === "2" ? "item attached panels active tab segment":"item attached panels tab segment"}
                         data-tab="tab-2"
                         data-id="2"
                         onClick={ (e) => this._handleClick(e) }
                    >
                        <Icon name="wrench" color="teal" corner/>
                        Panel Productividad</div>
                </div>
                { this.tabPanels() }
            </div>
        )
    }
}