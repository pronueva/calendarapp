import React from 'react';
import { connect } from 'react-redux';
import { Grid, Divider } from 'semantic-ui-react';
import { styles , calendarDate } from '../../../../../utils/base'; //base del componente
import { default as diff } from 'deep-diff';

//utils
import { getInfoTable } from '../../../utils/productionTablesFunctions';

const infoTable = getInfoTable();
class ProductionPanel extends React.Component{
    constructor(props){
        super(props);
    }
    componentWillMount(){
        const { options , shifts , assignment, weeks } = this.props;
        const { currentYear , currentMonth } = this.props.date;
        const days = new Date(currentYear , currentMonth+1 , 0 ).getDate(); // numero de dias del mes
        const firstDayOfMonth = new Date(currentYear , currentMonth, 1).getDay(); // primer dia del mes
        this.setState({
            weeks,
            options,
            shifts,
            assignment,
            days,
            firstDayOfMonth
        });
    }
    componentWillReceiveProps(nextProps){
        this.setState({ assignment: nextProps.assignment  });
    }
    render(){
        return(
            <div className="scrollable">
                <h1>Panel de Productividad</h1>
                <Divider />
                <Grid centered divided  columns={ this.props.used_weeks } className="month">
                    <Month weeks={this.state.weeks}/>
                    {
                        infoTable.map( (i) => {
                            return(
                                <MonthPanel key={i.id} info={i} weeks={this.state.weeks} assignment={this.state.assignment}/>
                            )
                        })
                    }
                </Grid>
            </div>
        )
    }
}
class Month extends React.Component{
    renderWeeks(){
        return this.props.weeks.map( week  => {
            if(week){
                return(
                    <Grid.Column key={ week.id } className="wide week">
                        <Week days={ week.days } week={ week } month={ week.month } year={ week.year } />
                    </Grid.Column>
                )
            }
        })
    }
    render(){
        return(
            <Grid.Row className="week-row">
                <div className="wide employee column week">Nomina</div>
                {this.renderWeeks()}
            </Grid.Row>
            )
    }
}
class Week extends React.Component{
    renderDays(){
        return this.props.days.map( day => {
            if(day){
                return(
                    <Days day={ day } key={day.number} />
                )
            }
        })
    }
    render(){
        return(
            <Grid columns="equal">
                { this.renderDays() }
                <Grid.Column style={styles.calendarDayGrid}>
                    <p className="month"></p>
                    <p className="month">+</p>
                </Grid.Column>
            </Grid>
        )
    }

}
class Days extends React.Component{
    dayName( day ){
        return calendarDate.weekdaysMin[ day ]
    }
    render(){
        return(
            <Grid.Column style={styles.calendarDayGrid}>
                <p className={this.props.day.is_month_day} >{ this.dayName( this.props.day.day ) }</p>
                <p className="month number" >{ this.props.day.number }</p>
            </Grid.Column>
        )
    }

}

class MonthPanel extends React.Component{
    weeksPanel(){
        return this.props.weeks.map( week  => {
            if(week){
                return(
                    <Grid.Column key={ week.id } className="wide week">
                        <WeekPanel key={ week.id }
                                   days={ week.days }
                                   week={ week }
                                   month={ week.month }
                                   year={ week.year }
                                   assignment={ this.state.assignment }
                                   info={ this.state.info }
                        />
                    </Grid.Column>
                )
            }
        })
    }
    componentWillMount(){
        const { weeks , info , assignment } = this.props;
        this.setState({
            weeks,
            info,
            assignment
        })
    }
    shouldComponentUpdate(nextProps,nextState){
        let difference = diff( this.state.assignment.production[this.state.info.id]  , nextState.assignment.production[this.state.info.id] );
        if(typeof difference !== "undefined"){
            let dd = difference.filter(function (dd) {
                if(dd.path[dd.path.length-1] === "value"){
                    return dd.kind === "E";
                }
            });

            return  typeof dd !== "undefined" ? dd.length > 0 : false;
        }
        return false
    }
    componentWillReceiveProps(nextProps){
        this.setState({ assignment: nextProps.assignment  });
    }
    render(){
        return(
            <Grid.Row className="week-row">
                <div className="wide employee column week" style={{fontWeight:'bold',fontSize:14}}>{this.props.info.text}</div>
                {this.weeksPanel()}
            </Grid.Row>
        )
    }
}
class WeekPanel extends React.Component{
    dayPanel(){
        return this.state.days.map( day => {
            if(day){
                return(
                    <DayPanel key={day.number}
                                 day={day} week={this.props.week} month={day.month}
                                 assignment={this.props.assignment}
                                 info={this.props.info}
                    />
                )
            }
        })
    }
    componentWillMount(){
        const { days , week , info , assignment } = this.props;
        this.setState({
            days,
            week,
            info,
            assignment
        });
    }
    shouldComponentUpdate(nextProps,nextState){
        let difference = diff( this.state.assignment.production  , nextState.assignment.production );
        let updatable = 0;
        if(typeof difference !== "undefined"){
            difference.map( (dif) => {
                this.state.days.map( (d) => {
                    if(d.day_index === dif.path[2] ){
                        updatable = 1
                    }
                });
            });
        }
        return updatable === 1;
    }
    componentWillReceiveProps(nextProps){
        this.setState({ assignment: nextProps.assignment  });
    }
    render(){
        return(
            <Grid columns="equal">
                { this.dayPanel() }
                <Grid.Column style={styles.calendarDayGrid}>
                    <div className={"weekly_sum "}>-</div>
                </Grid.Column>
            </Grid>
        )
    }
}
class DayPanel extends React.Component{
    constructor(){
        super();
        this.state = {
            sum: 0
        }
    }
    componentWillMount(){
        const { assignment , day } = this.props;
        this.setState({
            assignment,
            day
        });
    }
    componentWillReceiveProps(nextProps){
        let difference = diff( this.state.assignment.production  , nextProps.assignment.production );
        if(typeof difference !== "undefined"){
            let dd = difference.filter(function (dd) {
                if(dd.path[dd.path.length-1] === "value"){
                    return dd.kind === "E";
                }
            });
            dd.map( (d) => {
                //&& this.props.info.id === difference[0].path[0]
                let new_sum = nextProps.assignment.production[this.props.info.id].allocations[d.path[2]].value;
                if(d.path[2] === this.state.day.day_index){
                    this.setState({
                        sum: new_sum
                    })
                }
            });

        }
    }
    render(){
        const sumStyle = this.state.sum > 0 ? styles.shiftPanel.success : styles.shiftPanel.default;
        return(
            <Grid.Column style={styles.calendarDayGrid}>
                <div style={Object.assign({}, styles.shiftPanel.general, sumStyle)}>
                    {this.state.sum}
                </div>
            </Grid.Column>
        )
    }
}

//connect
const mapStateToProps = state => ({
    date: state.date,
    assignment: state.assignment,
    options: state.shifts.options,
    shifts: state.shifts.shifts,
    employees: state.area.employees
});

export default connect(mapStateToProps,null)(ProductionPanel);