import React from 'react';
import { Loader , Dimmer } from 'semantic-ui-react';

export class LoaderComponent extends React.Component{
    render(){
        return(
            <Dimmer active={this.props.loading} inverted>
                <Loader inverted>Cargando ...</Loader>
            </Dimmer>
        )
    }
}