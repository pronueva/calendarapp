import React from 'react';
import { Grid , Image , Loader , Dimmer } from 'semantic-ui-react';

export class LoadingDataComponent extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            this.props.type === 1 ?
                <Grid.Row className="week-row">
                    <Dimmer active inverted>
                        <Loader content="Cargando datos ..."  />
                    </Dimmer>
                    <Grid.Row className="week-row">
                        <Image src={window.App.paragraph_src}  />
                    </Grid.Row>
                </Grid.Row>
            :
                <Grid.Row className="week-row">
                    <Grid.Row className="week-row">
                        <Image src={window.App.paragraph_src}  />
                    </Grid.Row>
                </Grid.Row>
        )

    }
}