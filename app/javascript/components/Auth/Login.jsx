import React from 'react';
import reqwest from 'reqwest';
import renderHTML from 'react-render-html';

import { Button , Icon , Input , Form } from 'semantic-ui-react';


export class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            canSubmit: true,
            email: '',
            password: '',
            error: '',
            loading: false
        }
    }
    submit(){
        this.setState({ loading: true })
        reqwest({
            url: '/users/sign_in.json',
            method: 'POST',
            data: {
                user:{
                    email: this.state.email,
                    password: this.state.password
                }
            },
            headers:{
                'X-CSRF-TOKEN': window.App.token
            }
        }).then(data =>{
            this.setState({ loading: true });
            this.reload();
        }).catch(err => {
            setTimeout(() => {
                this.setState({ loading: false });
                this.handleError(err);
            }, 1500);
        })
    }
    handleError(err){
        const errorMessage = JSON.parse(err.response).error;

        this.setState({
            error: errorMessage
        })
    }
    reload(){
        setTimeout(function(){
            location.reload();
        },1500)
    }
    syncField(e,fieldName){
        let element = e.target;
        let value = element.value;
        let json = {};
        json[fieldName] = value;
        this.setState(json);
    }
    render(){
        let errMessage = ''
        if (this.state.error !== '') {
            errMessage = '<div class="ui small message red"><div class="header"><i class="icon warning cicle"></i> Ha ocurrido un error</div>'+this.state.error+'</div>'
        }
        return(
            <Form loading={this.state.loading}>
                {renderHTML(errMessage)}
                <Form.Field>
                    <label>Correo Electrónico</label>

                    <Input
                        icon='users' iconPosition='left'
                        type="email"
                        name="email"
                        onChange={ (e)=>this.syncField(e , 'email') }
                        placeholder="Correo Electrónico" />
                </Form.Field>
                <Form.Field>
                    <label>Contraseña</label>
                    <Input
                        icon='lock' iconPosition='left'
                        type="password"
                        name="password"
                        onChange={ (e)=>this.syncField(e , 'password') }
                        placeholder="Contraseña" />
                </Form.Field>
                <Button
                    type="button" color="blue" onClick={ () => this.submit() }  >
                    <Icon name="cloud" />
                    Ingresar</Button>
            </Form>
        )
    }
}