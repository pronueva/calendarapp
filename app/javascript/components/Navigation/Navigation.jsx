import React, { Component } from 'react';
import reqwest from 'reqwest';
import { Menu , Dropdown , Icon , Loader } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

export class Navigation extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeItem: 'Dashboard',
            loading: false
        }
        this.handleLogOut = this.handleLogOut.bind(this);
    }
    handleLogOut(){
        this.setState({ loading: true });
            reqwest({
                url: '/users/sign_out.json',
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': window.App.token
                }
            }).then(data => {
                window.location.href = window.location.href
            }).catch(err => {
                console.log(err);
            })
    }
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
        const { activeItem } = this.state;

        return (
            <div>
                <Menu stackable inverted>
                    <Menu.Item>
                        <img src='/assets/logo-6a7faecab92c102b80e734a18e0e8f3a736f4a4a0b8300fef0e7bffec0a770cc.png' />
                    </Menu.Item>
                    <Dropdown className="inverted" item text='Opciones' simple>
                        <Dropdown.Menu>
                            <Dropdown.Item>
                                <NavLink to="/" >Home</NavLink>
                            </Dropdown.Item>
                            <Dropdown.Item>
                                <NavLink to="/usuarios" >Usuarios</NavLink>
                            </Dropdown.Item>
                            <Dropdown.Item>
                                <NavLink to="/dashboard" >Dashboard</NavLink>
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <Dropdown className="inverted" item text='Asignaciones' simple icon="table">
                        <Dropdown.Menu>
                            <Dropdown.Item>
                                <NavLink to="/asignaciones" >Nueva Asignación</NavLink>
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <Menu.Item position='right'>
                        <a href="#/" onClick={() => this.handleLogOut()}>
                            <Icon name="power"/>
                            { this.state.loading ? <Loader active inline /> : "Cerrar Sesión"}
                        </a>
                    </Menu.Item>
                </Menu>
            </div>
        )
    }
}
