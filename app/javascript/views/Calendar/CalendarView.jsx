import React from 'react';
import { connect } from 'react-redux';
import reqwest from 'reqwest';
import { default as diff } from 'deep-diff';

import { Segment , Grid , Card , Icon , Image , Feed } from 'semantic-ui-react';
import { LoaderComponent } from "../../components/LoaderComponent/LoaderComponent";

//utils
import { calendarDate , getBaseURL } from '../../utils/base';

//actions
import { set_current_year , set_current_month , set_current_day } from '../../actions/date';
import { toggle_loading } from '../../actions/loading';
import { fetch_areas , remove_area } from '../../actions/area';
import { fetch_departments } from '../../actions/department';
import { fetch_shifts } from '../../actions/shifts';
import { fetch_benefits } from '../../actions/benefits';



class CalendarView extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            shifts: '',
            loading: this.props.loading,
            benefits: '',
            calendar_logs: {
                pending_assignments:{
                    key:1,title:"Nuevas Asignaciones",
                    meta:"Áreas sin asignar en este mes",
                    areas:[]
                },
                unpublished_assignments:{
                    key:2,title:"Asignaciones Pendientes de Aprobación",
                    meta:"Áreas con asignaciones realizadas pendientes de Aprobación",
                    areas:[]
                },
                published_assignments:{
                    key:3,title:"Asignaciones Aprobadas",
                    meta:"Áreas publicadas",
                    areas:[]
                },
            },
        }
    }
    componentWillMount(){
        if(this.props.date){
            this.props.set_current_year(this.props.date.getFullYear());
            this.props.set_current_month(this.props.date.getMonth());
            this.props.set_current_day(this.props.date.getDate());
        }else{
            const date = new Date();
            this.props.set_current_year(date.getFullYear());
            this.props.set_current_month(date.getMonth());
            this.props.set_current_day(date.getDate());
        }
        this.props.fetch_areas();
        this.props.fetch_departments();

        this.props.shifts.length === 0 ? this.props.fetch_shifts() : "";
        this.props.benefits.length === 0 ? this.props.fetch_benefits() : "";
        if(this.props.area){
            this.props.remove_area();
        }
        this.setState({
            areas: this.props.areas
        });

        this.getCalendarLogs();
    }
    shouldComponentUpdate(nextProps,nextState){
        return typeof diff(this.state , nextState) !== "undefined";
    }

    componentWillUpdate(nextProps,nextState){
        if( typeof diff(this.state.areas , nextState.areas) !== "undefined" ){
            this.getCalendarLogs();
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            areas: nextProps.areas
        })
    }
    getCalendarLogs(){
        this.setState({
            loading: true
        });
        reqwest({
            url: window.ENV.API_URL + 'list_calendar_logs_date/2017-12-1',
            method: 'GET',
        }).then(data =>{
            this.getAreasByLogs(data);
        }).catch(err => {
            this.getAreasByLogs();
        })
    }
    getAreasByLogs(data){
        let published_assignments , unpublished_assignments = [];
        if(this.state.areas.length > 0 ){
            const { areas , calendar_logs } = this.state;
            if(data){
                areas.filter( (area) => {
                    let returnable;
                    for(let i = 0; i < data.length; i++){
                        if(area.id !== data[i].area_id){
                            returnable = true;
                        }else{
                            returnable = false;
                            break;
                        }
                        if( i === data.length ){
                            return returnable;
                        }
                    }
                    if(returnable){
                        calendar_logs.pending_assignments.areas.push(area);
                    }
                });
                data.map( (d) => {
                    if(d.published){
                        published_assignments = areas.filter( (area) => {
                            return area.id === d.area_id
                        });
                        calendar_logs.published_assignments.areas.push(published_assignments[0]);
                    }else{
                        unpublished_assignments = areas.filter( (area) => {
                            return area.id === d.area_id
                        });
                        calendar_logs.unpublished_assignments.areas.push(unpublished_assignments[0]);
                        let last = calendar_logs.unpublished_assignments.areas.length - 1 ;
                        calendar_logs.unpublished_assignments.areas[last].code = d.code;
                    }
                });
            }else{
                const { areas , calendar_logs } = this.state;
                areas.filter( (area) => {
                    calendar_logs.pending_assignments.areas.push(area);
                });
            }
            this.setState({
                loading: false,
                calendar_logs
            });
        }
    }
    _handleClick(e,code){
        e.preventDefault();
        let url;
        if(code){
            url = getBaseURL() + '/calendarApp/#/'+e.currentTarget.id + '/' + code;
        }else{
            url= getBaseURL() + '/calendarApp/#/'+e.currentTarget.id + '/ ';
        }
        window.location.href = url;
    }
    render(){
        return(
            <div>
                <Segment.Group>
                    <Segment>
                        <Grid columns={2} className="row">
                            <Grid.Column>
                                <h1 className="header">Asignación Horario</h1>
                            </Grid.Column>
                        </Grid>
                    </Segment>
                </Segment.Group>
                <br/>
                <div className="ui content segment">
                    <LoaderComponent loading={ this.state.loading } />
                    <Grid celled="internally">
                        <Grid.Row width={16} centered style={{paddingBottom: "1rem"}}>
                            <h2>{ calendarDate.Months[this.props.currentMonth] + ' ' + this.props.currentYear  }</h2>
                            <p className="info"><Icon name="info"/>Seleccione un área</p>
                        </Grid.Row>
                        <Grid.Row style={{paddingTop: "1rem"}} columns={3}>
                                { this.state.calendar_logs ?
                                    Object.keys(this.state.calendar_logs).map( (clog) => {
                                        let calendar = this.state.calendar_logs[clog];
                                            return(
                                                this.state.calendar_logs[clog] ?
                                                    <Grid.Column className="assignment" key={calendar.key}>
                                                        <Card>
                                                            <Card.Content>
                                                                <Image floated='right' size='mini' src={window.App.calendar_flat_icon_src} />
                                                                <Card.Header>
                                                                    <h3 className="assignment header">{calendar.title}</h3>
                                                                </Card.Header>
                                                                <Card.Meta>
                                                                    {calendar.meta}
                                                                </Card.Meta>
                                                            </Card.Content>
                                                            <Card.Content style={{backgroundColor: "#d4dfff"}}>
                                                                <Feed>
                                                                    {
                                                                        calendar.areas.map( (area) => {
                                                                            const department = this.props.departments.filter( (d) => {
                                                                                return d.id === area.department_id;
                                                                            });
                                                                            const code = area.code;
                                                                            return(
                                                                                <Feed.Event key={area.id} className="assignment">
                                                                                    <Feed.Content>
                                                                                        <Feed.Date content={department[0].name}/>
                                                                                        <Feed.Summary>
                                                                                            <p style={{textTransform:"capitalize"}}>{area.name}</p>
                                                                                        </Feed.Summary>
                                                                                    </Feed.Content>
                                                                                    <Feed.Label>
                                                                                        <div className="label monthLink" id={area.id} onClick={ (e) => this._handleClick(e,code) } >
                                                                                            <Icon name="external"/>
                                                                                        </div>
                                                                                    </Feed.Label>
                                                                                </Feed.Event>
                                                                                )
                                                                        })

                                                                    }

                                                                </Feed>
                                                            </Card.Content>
                                                        </Card>
                                                    </Grid.Column>
                                                    :""
                                            )
                                       })
                                :""}
                        </Grid.Row>
                    </Grid>

                </div>
            </div>
        )
    }
}
//connect
const mapStateToProps = state => ({
    departments: state.departments,
    department: state.department,
    areas: state.areas,
    area: state.area,
    shifts: state.shifts,
    benefits: state.benefits,
    currentYear: state.date.currentYear,
    currentMonth: state.date.currentMonth,
    currentDay: state.date.currentDay,
    loading: state.loading
});
const mapDispatchToProps = (dispatch) => {
    return {
        set_current_year: (year) => {
            dispatch(set_current_year(year));
        },
        set_current_month: (month) => {
            dispatch(set_current_month(month));
        },
        set_current_day: (day) => {
            dispatch(set_current_day(day));
        },
        fetch_areas: () => {
            dispatch(fetch_areas());
        },
        fetch_departments: () => {
            dispatch(fetch_departments());
        },
        remove_area: () => {
            dispatch(remove_area());
        },
        fetch_shifts: () => {
            dispatch(fetch_shifts());
        },
        fetch_benefits: () => {
            dispatch(fetch_benefits());
        },
        toggle_loading: () => {
            dispatch(toggle_loading());
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(CalendarView)
