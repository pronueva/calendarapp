import React from 'react';
import { connect } from 'react-redux';
import reqwest from 'reqwest';

import { CalendarTool } from '../../../components/CalendarTool'
import { LoadingDataComponent } from '../../../components/LoaderComponent/LoadingDataComponent'

import { Segment , Grid , Button } from 'semantic-ui-react';

//utils
import { getBaseURL } from '../../../utils/base';

//actions
import { toggle_loading } from '../../../actions/loading';
import { set_current_year , set_current_month , set_current_day } from '../../../actions/date';
import { fetch_areas , fetch_area } from '../../../actions/area';
import { fetch_shifts } from '../../../actions/shifts';
import { fetch_benefits } from '../../../actions/benefits';


class NewCalendar extends React.Component{
    constructor(){
        super();
        this.state = {
            benefits: [],
            shifts: [],
            calendar: null,
            area:{
                id:null,
                code:null
            }
        }
    }
    componentWillMount(){
        const { area } = this.state;
        area.id = this.props.match.params.area_id;
        area.code = this.props.match.params.area_code;
        this.setState({
            area
        }) ;
        if(area.code){
            this.getCalendarData(area.code);
        }
        this.props.fetch_area(area.id);
        this.props.shifts.length === 0 ? this.props.fetch_shifts() : "";
        this.props.benefits.length === 0 ? this.props.fetch_benefits() : "";
        console.log(this.props)
        console.log("-----------")
    }
    componentDidMount(){
        if(this.props.date.currentMonth === 0 && this.props.currentYear === 0){
            window.location.href = getBaseURL() + '/calendarApp/#'
        }
    }
    shouldComponentUpdate(nextProps, nextState){
        if(this.state !== nextState){
            if(this.state.benefits.length > 0 && this.state.shifts.length > 0 ) {
                if(document.getElementById("calendarContainer").offsetHeight > 300)
                    document.getElementById("scrollable").style.height = document.getElementById("calendarContainer").offsetHeight+120+"px";
            }
            return true;
        }
        return false;
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            benefits: nextProps.benefits,
            shifts: nextProps.shifts.shifts
        });
    }
    getCalendarData(code){
        reqwest({
            url: window.ENV.API_URL+'List_calendars_code/'+code,
            method: "GET",
        }).then(data =>{
            this.setState({
                calendar: data
            });
        }).catch(err => {
            console.log(err)
        });
    }
    _handleClick(){
        const { assignment } = this.props;
        reqwest({
            url: window.ENV.API_URL+'calendars',
            method: "POST",
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), // Optional
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({calendar: assignment })
        }).then(data =>{
            console.log(data)
        }).catch(err => {
            console.log(err)
        })
    }
    render(){
        return(
            <div>
                <Segment.Group>
                    <Segment>
                        <Grid columns={2} className="row">
                            <Grid.Column>
                                <h1 className="header">Asignación de Turnos </h1>
                            </Grid.Column>
                            <Grid.Column>
                                <Button basic color="teal" onClick={ ()=>this._handleClick()}>Guardar</Button>
                            </Grid.Column>
                        </Grid>
                    </Segment>
                </Segment.Group>
                <div className="ui content segment" style={{backgroundColor:"#f9f9f9"}}>
                    {
                        this.state.benefits.length > 0 && this.state.shifts.length > 0 ?
                            <CalendarTool benefits={this.state.benefits} shifts={this.state.shifts} calendar={this.state.calendar}/>
                            :
                            <LoadingDataComponent type={1} />
                    }
                </div>
            </div>
        )
    }
}
//connect
const mapStateToProps = state => ({
    areas: state.areas,
    area: state.area,
    assignment: state.assignment,
    shifts: state.shifts,
    benefits: state.benefits,
    currentYear: state.date.currentYear,
    currentMonth: state.date.currentMonth,
    currentDay: state.date.currentDay,
    loading: state.loading,
    date: state.date
});
const mapDispatchToProps = (dispatch) => {
    return {
        set_current_year: (year) => {
            dispatch(set_current_year(year));
        },
        set_current_month: (month) => {
            dispatch(set_current_month(month));
        },
        set_current_day: (day) => {
            dispatch(set_current_day(day));
        },
        fetch_areas: () => {
            dispatch(fetch_areas());
        },
        fetch_area: (area_id) => {
            dispatch(fetch_area(area_id));
        },
        fetch_shifts: () => {
            dispatch(fetch_shifts());
        },
        fetch_benefits: () => {
            dispatch(fetch_benefits());
        },
        toggle_loading: () => {
            dispatch(toggle_loading());
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(NewCalendar)