import * as types from '../actions/actionTypes';

export default function reducer( state = null , action = {}){
    switch(action.type){
        case types.FETCH_DEPARTMENT:
            return action.department;
        default:
            return state;
    }
}
