import update from 'react-addons-update';
import * as types from '../actions/actionTypes';

export default function reducer( state = null , action = {}){
    switch(action.type){
        case types.FETCH_AREA:
            return action.area;
        case types.UPDATE_EMPLOYEE_BENEFIT:
            const data = action.data;
            let employee_arr_key = 0;
            for(let i = 0; i < state.employees.length; i++){
                if( state.employees[i].id === data.employee.id ){
                    employee_arr_key = i;
                }
            }
            return update(state,{
                employees:{
                    [employee_arr_key]: {
                        benefit: data.benefit
                    }
                }
            });
        case types.REMOVE_AREA:
            return update(state, {$set: null} );
        default:
            return state;
    }
}
