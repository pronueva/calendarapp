import { combineReducers } from 'redux';

import calendar from './calendar';
import date from './date';
import loading from './loading';
import area from './area';
import areas from './areas';
import department from './department';
import departments from './departments';
import shifts from './shifts';
import benefits from './benefits';
import assignment from './assignment';

export default combineReducers({
    date,
    area,
    areas,
    department,
    departments,
    shifts,
    benefits,
    assignment,
    calendar,
    loading
})