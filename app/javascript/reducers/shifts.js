import * as types from '../actions/actionTypes';

export default function reducer( state = [] , action = {}){
    switch(action.type){
        case types.FETCH_SHIFTS:
            const shifts = action.shifts;
            return {
                ...state,
                shifts
            };
        case types.SET_SHIFTS_OPTIONS:
            const options = action.options;
            return {
                ...state,
                options
            };
        default:
            return state;
    }
}
