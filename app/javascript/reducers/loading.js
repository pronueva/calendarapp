import * as types from '../actions/actionTypes';

export default function reducer( state = true , action = {}){
    switch(action.type){
        case types.TOGGLE_LOADING:
            if(action.loading){
                return state;
            }else{
                return false;
            }
        default:
            return state;
    }
}