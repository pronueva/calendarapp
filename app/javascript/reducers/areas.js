import * as types from '../actions/actionTypes';

export default function reducer( state = [] , action = {}){
    switch(action.type){
        case types.FETCH_AREAS:
            return action.areas;
        default:
            return state;
    }
}
