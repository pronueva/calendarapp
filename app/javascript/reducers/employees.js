import * as types from '../actions/actionTypes';

export default function reducer( state = [] , action = {}){
    switch(action.type){
        case types.SET_EMPLOYEES:
            return action.employees;
        default:
            return state;
    }
}