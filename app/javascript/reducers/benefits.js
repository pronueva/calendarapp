import * as types from '../actions/actionTypes';

export default function reducer( state = [] , action = {}){
    switch(action.type){
        case types.FETCH_BENEFIT:
            const benefits = action.benefit;
            return {
                ...state,
                benefits
            };
        case types.FETCH_BENEFITS:
            return action.benefits;
        default:
            return state;
    }
}
