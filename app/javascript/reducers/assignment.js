import update from 'react-addons-update';
import * as types from '../actions/actionTypes';

export default function reducer( state = {} , action = {}){
    switch(action.type){
        case types.UPDATE_ASSIGNMENT:
            const new_data = action.data;
            const employee_id = new_data.employee;
            const new_day_index = new_data.day_index;
            const worked_day = new_data.worked_day;
            const new_shift = new_data.shift ? new_data.shift : state.employees[employee_id].allocations[new_day_index].shift;
            return update(state,{
                employees:{
                    [employee_id]: {
                        allocations:{
                            [new_day_index]:{
                                shift: {$set: new_shift},
                                worked_day: {$set: worked_day},
                                status: {$set: new_data.status ? new_data.status : 0},
                                err_msg: {$set: new_data.err_msg ? new_data.err_msg : null}
                            }
                        }
                    }
                }
            });
        case types.UPDATE_WORKED_SUNDAYS:
            let data = action.data;
            return update(state,{
                employees:{
                    [data.employee]: {
                        weeks:{
                            [data.week]: {
                                is_sunday_worked: {$set: data.worked_day },
                                is_seventh_day_worked: {$set: data.is_seventh_day_worked ? 1 : 0 },
                                weekly_sum: {$set: 0 }
                            }
                        }
                    }
                }
            });
        case types.UPDATE_PRODUCTION_PANEL_DATA:
            data = action.data;
            let key = 0;
            state.production.map((p) =>{
                if(p.text === data.production_key){
                    key = p.id;
                }
            });
            return update(state,{
                production:{
                    [key]: {
                        allocations:{
                            [data.day_index]: {
                                value: {$set: state.production[key].allocations[data.day_index].value >= 0 ? state.production[key].allocations[data.day_index].value + data.value : 0 },
                            }
                        }
                    }
                }
            });
        case types.SET_PRODUCTION_PANEL_STRUCTURE:
            return update(state,{
                production:
                    {$set: action.data}
            });
        case types.SET_ASSIGNMENT:
            const assignment = action.data;
            let new_state = state;
            let i = 1;
            assignment.map( (a) => {
                let date = a.date.split('-');
                let employee_id = a.employee_id;
                const year = date[0];
                const month = date[1];
                const day_number = date[2];
                const day = new Date(year+'-'+month+'-'+day_number).getDay();

                new_state.employees[employee_id].allocations[i].shift = a.shift_id;
                new_state.employees[employee_id].allocations[i].status = 1;
                i ++;
            });
            return new_state;
        case types.UPDATE_SUM_HOURS:
            console.log(state)
            console.log(action)
            return update(state,{
                employees:{
                    [action.data.employee]: {
                        weeks:{
                            [action.data.week_id]: {
                                weekly_sum: {$set: action.data.weekly_sum },
                            }
                        }
                    }
                }
            });

        case types.SET_ASSIGNMENTS_STRUCTURE:
            return action.data;
        case types.SET_NULL_ASSIGNMENT:
            return action.data;
        default:
            return state;
    }
}
