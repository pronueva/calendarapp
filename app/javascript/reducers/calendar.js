import update from 'react-addons-update';
import * as types from '../actions/actionTypes';

export default function reducer( state = [] , action = {}){
    switch(action.type){
        case types.SET_CALENDAR_DEPARTMENT_ID:
            return action.department_id;
        case types.SET_CURRENT_MONTH_WEEKS:
            const weeks = action.weeks;
            return  {
                ...state,
                weeks
            };
        case types.SET_CURRENT_MONTH_USED_WEEKS:
            const used_weeks = action.used_weeks;
            return  {
                ...state,
                used_weeks
            };
        case types.SET_CURRENT_MONTH_NAME:
            const month_name = action.month_name;
            return  {
                ...state,
                month_name
            };
        default:
            return state;
    }
}