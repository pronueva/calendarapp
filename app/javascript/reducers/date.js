import * as types from '../actions/actionTypes';

const initialState = {
    currentYear: 0,
    currentMonth: 0,
    currentDay: 0
};

export default function reducer( state = initialState , action){
    switch(action.type){
        case types.SET_CURRENT_YEAR:
            return{
                currentYear: action.year,
                currentMonth: state.currentMonth,
                currentDay: state.currentDay
            }
        case types.SET_CURRENT_MONTH:
            return{
                currentYear: state.currentYear,
                currentMonth: action.month,
                currentDay: state.currentDay
            }
        case types.SET_CURRENT_DAY:
            return{
                currentYear: state.currentYear,
                currentMonth: state.currentMonth ,
                currentDay: action.currentDay
            }

        default:
            return state;
    }
}