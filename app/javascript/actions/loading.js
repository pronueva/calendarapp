import * as types from './actionTypes';

export function toggle_loading(loading)
{
    return {
        type: types.TOGGLE_LOADING,
        loading
    }
}