import * as types from "./actionTypes";
import {toggle_loading} from "./loading";

export function set_area(area)
{
    return {
        type: types.FETCH_AREA,
        area
    }
}
export function set_areas(areas)
{
    return {
        type: types.FETCH_AREAS,
        areas
    }
}
export function remove_area()
{
    return {
        type: types.REMOVE_AREA
    }
}
export function update_employee_benefit(data){
    return{
        type: types.UPDATE_EMPLOYEE_BENEFIT,
        data
    }
}
export function fetch_employee_benefit(employee){
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_benefits/'+employee.benefit_id+'.json')
            .then(res => res.json())
            .then( data => {
                let dataArr = {
                    benefit: data,
                    employee: employee
                }
                dispatch(update_employee_benefit(dataArr));
            })
    }
}
export function fetch_area(area)
{
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_areas/'+area+'.json', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'authorization': 'Token token='+window.App.token,
                    'X-CSRFToken': window.App.token,
                    'Host': window.ENV.API_HOST
                }
            })
            .then(res => res.json())
            .then(data => {
                dispatch(set_area(data));
                dispatch(toggle_loading());
            })
    }
}
export function fetch_areas()
{
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_areas', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'authorization': 'Token token='+window.App.token,
                    'X-CSRFToken': window.App.token,
                    'Host': window.ENV.API_HOST
                }
            })
            .then(res => res.json())
            .then(data => {
                dispatch(toggle_loading());
                dispatch(set_areas(data));
            })
    }
}