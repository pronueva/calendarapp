import * as types from './actionTypes';

export function set_shifts(shifts){
    return{
        type: types.FETCH_SHIFTS,
        shifts
    }
}
export function fetch_shifts()
{
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_shifts', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'authorization': 'Token token='+window.App.token,
                    'X-CSRFToken': window.App.token,
                    'Host': window.ENV.API_HOST
                }
            })
            .then(res => res.json())
            .then( data => {
                dispatch(set_shifts(data));
            })
    }
}

export function set_shifts_options(options){
    return {
        type: types.SET_SHIFTS_OPTIONS,
        options
    }
}
