import * as types from './actionTypes';

export function set_employees(employees)
{
    return {
        type: types.SET_EMPLOYEES,
        employees
    }
}
export function fetch_employees(data)
{
    return set_employees(data)
}