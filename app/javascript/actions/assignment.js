import * as types from './actionTypes';

export function set_assignments_structure(data){
    return{
        type: types.SET_ASSIGNMENTS_STRUCTURE,
        data
    }
}
export function set_production_panel_structure(data){
    return{
        type: types.SET_PRODUCTION_PANEL_STRUCTURE,
        data
    }
}
export function update_production_panel_data(data){
    return{
        type: types.UPDATE_PRODUCTION_PANEL_DATA,
        data
    }
}
export function set_assignment(data){
    return{
        type: types.SET_ASSIGNMENT,
        data
    }
}
export function update_assignment(data){
    return{
        type: types.UPDATE_ASSIGNMENT,
        data
    }
}
export function update_sum_hours(data){
    return{
        type: types.UPDATE_SUM_HOURS,
        data
    }
}

export function update_worked_sundays(data){
    return{
        type: types.UPDATE_WORKED_SUNDAYS,
        data
    }
}
