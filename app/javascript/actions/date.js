import * as types from './actionTypes';

export function set_current_year( year ){
    return{
        type: types.SET_CURRENT_YEAR,
        year
    }
}
export function set_current_month( month ){
    return{
        type: types.SET_CURRENT_MONTH,
        month
    }
}
export function set_current_day( day ){
    return{
        type: types.SET_CURRENT_DAY,
        day
    }
}