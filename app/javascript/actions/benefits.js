import * as types from './actionTypes';

export function set_benefit(benefit){
    return{
        type: types.FETCH_BENEFIT,
        benefit
    }
}
export function set_benefits(benefits){
    return{
        type: types.FETCH_BENEFITS,
        benefits
    }
}
export function fetch_benefit(benefit_id)
{
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_benefits/'+benefit_id+'.json')
            .then(res => res.json())
            .then( data => {
                dispatch(set_benefit(data));
            })
    }
}
export function fetch_benefits()
{
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_benefits.json')
            .then(res => res.json())
            .then( data => {
                dispatch(set_benefits(data));
            })
    }
}
