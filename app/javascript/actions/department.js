import * as types from "./actionTypes";
import {toggle_loading} from "./loading";

export function set_department(department)
{
    return {
        type: types.FETCH_DEPARTMENT,
        department
    }
}
export function set_departments(departments)
{
    return {
        type: types.FETCH_DEPARTMENTS,
        departments
    }
}
export function fetch_department(department)
{
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_departments/'+department+'.json', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'authorization': 'Token token='+window.App.token,
                    'X-CSRFToken': window.App.token,
                    'Host': window.ENV.API_HOST
                }
            })
            .then(res => res.json())
            .then(data => {
                dispatch(set_department(data));
                dispatch(toggle_loading());
            })
    }
}
export function fetch_departments()
{
    return dispatch => {
        fetch(window.ENV.API_URL + 'list_departments', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'authorization': 'Token token='+window.App.token,
                    'X-CSRFToken': window.App.token,
                    'Host': window.ENV.API_HOST
                }
            })
            .then(res => res.json())
            .then(data => {
                dispatch(toggle_loading());
                dispatch(set_departments(data));
            })
    }
}