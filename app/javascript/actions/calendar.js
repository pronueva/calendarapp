import * as types from './actionTypes';

export function set_calendar_department_id(department_id)
{
    return{
        type: types.SET_CALENDAR_DEPARTMENT_ID,
        department_id
    }
}
export function set_current_month_weeks(weeks)
{
    return{
        type: types.SET_CURRENT_MONTH_WEEKS,
        weeks
    }
}
export function set_current_month_used_weeks(used_weeks)
{
    return{
        type: types.SET_CURRENT_MONTH_USED_WEEKS,
        used_weeks
    }
}
export function set_current_month_name(month_name)
{
    return{
        type: types.SET_CURRENT_MONTH_NAME,
        month_name
    }
}
