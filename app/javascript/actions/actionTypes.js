//GENERAL ACTIONS
export const SET_SECTION                            = 'SET_SECTION';
export const SET_PATH                               = 'SET_PATH';
export const TOGGLE_LOADING                         = 'TOGGLE_LOADING';
//CALENDAR ACTIONS
export const SET_CALENDAR_DEPARTMENT_ID             = 'SET_CALENDAR_DEPARTMENT_ID';
export const SET_CURRENT_MONTH_USED_WEEKS           = 'SET_CURRENT_MONTH_USED_WEEKS';
export const SET_CURRENT_MONTH_NAME                 = 'SET_CURRENT_MONTH_NAME';
export const SET_CURRENT_MONTH_WEEKS                = 'SET_CURRENT_MONTH_WEEKS';
//DATE ACTIONS
export const SET_CURRENT_YEAR                       = 'SET_CURRENT_YEAR';
export const SET_CURRENT_MONTH                      = 'SET_CURRENT_MONTH';
export const SET_CURRENT_DAY                        = 'SET_CURRENT_DAY';
//AREA ACTIONS
export const FETCH_AREA                             = 'FETCH_AREA';
export const REMOVE_AREA                            = 'REMOVE_AREA';
export const UPDATE_EMPLOYEE_BENEFIT                = 'UPDATE_EMPLOYEE_BENEFIT';
export const FETCH_AREAS                            = 'FETCH_AREAS';
//DEPARTMENT ACTIONS
export const FETCH_DEPARTMENT                       = 'FETCH_DEPARTMENT';
export const FETCH_DEPARTMENTS                      = 'FETCH_DEPARTMENTS';
//SHIFTS ACTIONS
export const FETCH_SHIFTS                           = 'FETCH_SHIFTS';
export const SET_SHIFTS_OPTIONS                     = 'SET_SHIFTS_OPTIONS';
//BENEFIT ACTIONS
export const FETCH_BENEFIT                          = 'FETCH_BENEFIT';
export const FETCH_BENEFITS                         = 'FETCH_BENEFITS';
//ASSIGNMENTS ACTIONS
export const SET_ASSIGNMENT                         = 'SET_ASSIGNMENT';
export const SET_ASSIGNMENTS_STRUCTURE              = 'SET_ASSIGNMENTS_STRUCTURE';
export const SET_PRODUCTION_PANEL_STRUCTURE         = 'SET_PRODUCTION_PANEL_STRUCTURE';
export const UPDATE_PRODUCTION_PANEL_DATA           = 'UPDATE_PRODUCTION_PANEL_DATA';
export const UPDATE_ASSIGNMENT                      = 'UPDATE_ASSIGNMENT';
export const SET_NULL_ASSIGNMENT                    = 'UPDATE_ASSIGNMENT';
export const UPDATE_WORKED_SUNDAYS                  = 'UPDATE_WORKED_SUNDAYS';
export const UPDATE_SUM_HOURS                       = 'UPDATE_SUM_HOURS';
