import React from 'react'

const es_date =  require('./date_translations.json')["es"];

export const styles = {
    tableHeader:{
        paddingRight: "0"
    },
    list:{
        backgroundColor: "#f9f9f9",
        padding: "2rem",
        borderRadius: "2px",
        width:"100%",
    },
    headingMonth : {
        textTransform: "capitalize"
    },
    calendarDayGrid: {
        textAlign: "center",
        padding: "0px 2px",
        width: "calc(100%/8)"
    },
    dayCombobox: {
        paddingLeft: "0.5em",
        paddingRight: "0.5em",
        border: "1px solid rgba(0,0,0,0.05)",
        minWidth: "0px"
    },
    shiftPanel: {
        general:{
            fontSize:12
        },
        default:{
            color: "#999"
        },
        success:{
            color: "#000",
            fontWeight:"bold"
        }
    }
};
export const calendarDate = {
    Months : es_date["months"].split("_"),
    MonthsShort : es_date["monthsShort"].split("_"),
    WeekDays : es_date["weekdays"].split("_"),
    WeekdaysShort : es_date["weekdaysShort"].split("_"),
    weekdaysMin : es_date["weekdaysMin"].split("_")
};

export class Base extends React.Component{
    render(){
        return(
            <div></div>
        )
    }
}
export const freeShiftArr = {
    0: "D/L",
    1: "OFF",
    2: "VAC",
    3: "L/C",
    4: "LIC",
};
export function getBaseURL(){
    var pathArray = window.location.href.split('/');
    var protocol = pathArray[0];
    var host = pathArray[2];
    var url = protocol + '//' + host;

    return url;
}
