import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { HashRouter , Switch , Route } from 'react-router-dom';

import { createBrowserHistory } from 'history';
import configStore from '../store/configStore';

import { default as CalendarView } from '../views/Calendar/CalendarView';
import { default as NewCalendar  } from '../views/Calendar/NewCalendar/NewCalendar';

const store = configStore();
const history = createBrowserHistory();

export class CalendarContainer extends Component{

    render(){
        return(
        <Provider store={store}>
            <div>
                <HashRouter history={history}>
                    <Switch>
                        <Route exact path="/" name="Calendar" component={CalendarView}/>
                        <Route path="/:area_id/:area_code?" name="Calendar" component={NewCalendar}/>
                    </Switch>
                </HashRouter>
            </div>
        </Provider>
        )
    }
}