import React from 'react';
import WebpackerReact from 'webpacker-react';
import { Router, Route, Switch } from 'react-router-dom';

import { createBrowserHistory } from 'history';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

import { CalendarContainer } from '../containers/CalendarContainer';

const history = createBrowserHistory();

export class App extends React.Component{
    componentWillMount(){
        window.App.paragraph_src = this.props.paragraph_src;
        window.App.calendar_flat_icon_src = this.props.calendar_flat_icon;
    }
    render(){
        return(
                <CalendarContainer/>

        )
    }
}

WebpackerReact.setup({App});


