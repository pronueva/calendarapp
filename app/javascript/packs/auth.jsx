import React from 'react';
import WebpackerReact from 'webpacker-react';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

import {Login}  from '../components/Auth/Login'
import {Grid , Segment , Header , Image} from 'semantic-ui-react';

class Auth extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <Grid verticalAlign="middle" columns={1} centered className="full_height" >
                <Grid.Column className="six wide">
                    <Header as="h2" icon textAlign="center">
                        <Image src={this.props.logo_src} size='massive' wrapped/>
                        <Header.Content>
                            Iniciar Sesión
                        </Header.Content>
                    </Header>
                    <Segment>
                        <Login></Login>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

WebpackerReact.setup({Auth});

