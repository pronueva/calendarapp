class ApplicationMailer < ActionMailer::Base
  default from: 'sys@waki.com'
  layout 'mailer'
end
