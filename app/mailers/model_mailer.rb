class ModelMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.model_mailer.send_calendar_employee.subject
  #
  def change_account(user)
    @user = user
    mail to: @user.email, subject: "Se han realizado cambios en tu cuenta"
  end

  def send_email(user, message)
    @user = user
    mail to: @user.email, subject: message
  end
end
